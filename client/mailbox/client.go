package mailbox

import (
	"context"
	"errors"
	"log"
	"time"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/mailbox/model"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/utils"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/mailbox"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type mailboxClientInternalService struct {
	client pb.MailboxServiceClient
}
type MailboxInternalServices struct {
	IMailboxInternalService
	client       pb.MailboxServiceClient
	configClient InitMailboxClientParams
}

type InitMailboxClientParams struct {
	Url          string
	ExpireSecond int64
}

func NewMailboxClientInternalService(url string, expireRequest int64) *mailboxClientInternalService {
	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) * time.Second // Set your desired timeout duration
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	var (
		cred = insecure.NewCredentials()
	)

	conn, err := grpc.DialContext(ctx, url, grpc.WithTransportCredentials(cred))
	if err != nil {
		log.Printf("dial grpc to mailbox service error: %v", err)
		return nil
	}
	log.Println("dial grpc to mailbox service successfully")
	return &mailboxClientInternalService{
		client: pb.NewMailboxServiceClient(conn),
	}
}

func GetMailboxInternalService(url string, expireRequest int64) *MailboxInternalServices {
	var profileSvc *MailboxInternalServices

	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) // Set your desired timeout duration

	profileSvc = GetMailboxInternalServiceWithParams(utils.InitClientParams{
		Url:          url,
		ExpireSecond: int64(timeout),
	})
	log.Println("dial to mimiland profile internal service successfully")

	return profileSvc
}

func GetMailboxInternalServiceWithParams(params utils.InitClientParams, opts ...grpc.DialOption) *MailboxInternalServices {
	var svc *MailboxInternalServices

	if params.ExpireSecond == 0 {
		params.ExpireSecond = 120
	}
	timeout := time.Duration(params.ExpireSecond) * time.Second // Set your desired timeout duration

	// Create a context with the specified timeout
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	cred := insecure.NewCredentials()
	opts = append(opts, grpc.WithTransportCredentials(cred))
	retriableErrors := utils.DefaultRetriableErrors
	if len(params.RetriableErrors) > 0 {
		retriableErrors = append(retriableErrors, params.RetriableErrors...)
	}
	if params.RetryTime != 0 {
		unaryInterceptor := grpc_retry.UnaryClientInterceptor(grpc_retry.WithCodes(retriableErrors...), grpc_retry.WithMax(params.RetryTime), grpc_retry.WithPerRetryTimeout(params.PerRetryTimeout))
		opts = append(opts, grpc.WithUnaryInterceptor(unaryInterceptor))
	}
	conn, err := grpc.DialContext(ctx, params.Url, opts...)
	if err != nil {
		log.Println("cannot connect grpc mailboxSvc service")
		return nil
	}
	svc = &MailboxInternalServices{
		client: pb.NewMailboxServiceClient(conn),
		configClient: InitMailboxClientParams{
			Url:          params.Url,
			ExpireSecond: int64(timeout),
		},
	}
	log.Println("dial to mimiland mailbox internal service successfully")

	return svc
}

func (c *MailboxInternalServices) Ping(ctx context.Context) (message string, err error) {
	resp, err := c.client.Ping(ctx, &pb.MB_PingReq{})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	message = resp.Message
	return
}

func (c *MailboxInternalServices) DevCreateMails(ctx context.Context, in *model.CreateMailsRequest, opts ...grpc.CallOption) (resp *model.CreateMailsResponse, err error) {
	attachs := make([]*pb.MailBoxAttach, 0)
	for _, reqAttach := range in.Mail.Attachments {
		attMail := &pb.MailBoxAttach{
			Uid:      reqAttach.Uid,
			Type:     reqAttach.Type,
			Content:  reqAttach.Content,
			Name:     reqAttach.Name,
			Value:    reqAttach.Value,
			SourceId: reqAttach.SourceId,
		}
		attachs = append(attachs, attMail)
	}
	req := &pb.CreateMailsRequest{
		ClientId: in.ClientId,
		UserIds:  in.UserIds,
		Mail: &pb.MailBoxData{
			ClientId:    in.ClientId,
			From:        in.Mail.From,
			To:          in.Mail.To,
			Title:       in.Mail.Title,
			Body:        in.Mail.Body,
			ExpiredTime: in.Mail.ExpiredTime,
			Attachments: attachs,
			Status:      in.Mail.Status,
			TemplateUid: in.Mail.TemplateUid,
			SourceType:  in.Mail.SourceType,
			SourceId:    in.Mail.SourceId,
		},
	}
	pbResp, err := c.client.DevCreateMails(ctx, req, opts...)
	if err != nil {
		return
	}
	resp = &model.CreateMailsResponse{
		Code:    pbResp.Code,
		Message: pbResp.Message,
	}
	return resp, nil
}
func (c *MailboxInternalServices) ApiKeyCreateMails(ctx context.Context, in *model.CreateMailsRequest, opts ...grpc.CallOption) (resp *model.CreateMailsResponse, err error) {
	attachs := make([]*pb.MailBoxAttach, 0)
	for _, reqAttach := range in.Mail.Attachments {
		attMail := &pb.MailBoxAttach{
			Uid:      reqAttach.Uid,
			Type:     reqAttach.Type,
			Content:  reqAttach.Content,
			Name:     reqAttach.Name,
			Value:    reqAttach.Value,
			SourceId: reqAttach.SourceId,
		}
		attachs = append(attachs, attMail)
	}
	req := &pb.CreateMailsRequest{
		ClientId: in.ClientId,
		UserIds:  in.UserIds,
		Mail: &pb.MailBoxData{
			ClientId:    in.ClientId,
			From:        in.Mail.From,
			To:          in.Mail.To,
			Title:       in.Mail.Title,
			Body:        in.Mail.Body,
			ExpiredTime: in.Mail.ExpiredTime,
			Attachments: attachs,
			Status:      in.Mail.Status,
			TemplateUid: in.Mail.TemplateUid,
			SourceType:  in.Mail.SourceType,
			SourceId:    in.Mail.SourceId,
		},
	}
	pbResp, err := c.client.ApiKeyCreateMails(ctx, req, opts...)
	if err != nil {
		return
	}
	resp = &model.CreateMailsResponse{
		Code:    pbResp.Code,
		Message: pbResp.Message,
	}
	return resp, nil
}
func (c *MailboxInternalServices) InternalCreateMails(ctx context.Context, in *model.CreateMailsRequest, opts ...grpc.CallOption) (resp *model.CreateMailsResponse, err error) {
	attachs := make([]*pb.MailBoxAttach, 0)
	for _, reqAttach := range in.Mail.Attachments {
		attMail := &pb.MailBoxAttach{
			Uid:      reqAttach.Uid,
			Type:     reqAttach.Type,
			Content:  reqAttach.Content,
			Name:     reqAttach.Name,
			Value:    reqAttach.Value,
			SourceId: reqAttach.SourceId,
		}
		attachs = append(attachs, attMail)
	}
	req := &pb.CreateMailsRequest{
		ClientId: in.ClientId,
		UserIds:  in.UserIds,
		Mail: &pb.MailBoxData{
			ClientId:    in.ClientId,
			From:        in.Mail.From,
			To:          in.Mail.To,
			Title:       in.Mail.Title,
			Body:        in.Mail.Body,
			ExpiredTime: in.Mail.ExpiredTime,
			Attachments: attachs,
			Status:      in.Mail.Status,
			TemplateUid: in.Mail.TemplateUid,
			SourceType:  in.Mail.SourceType,
			SourceId:    in.Mail.SourceId,
		},
	}
	pbResp, err := c.client.InternalCreateMails(ctx, req, opts...)
	if err != nil {
		return
	}
	resp = &model.CreateMailsResponse{
		Code:    pbResp.Code,
		Message: pbResp.Message,
	}
	return resp, nil
}
func (c *MailboxInternalServices) DevCheckSentMailBoxsToSource(ctx context.Context, req *model.CheckSentMailBoxsToSourceReq, opts ...grpc.CallOption) (resp *model.CheckSentMailBoxsToSourceResp, err error) {
	pbReq := &pb.CheckSentMailBoxsToSourceReq{}
	pbReq.Sources = make([]*pb.SourceMailEvent, 0)
	for _, value := range req.Sources {
		pbReq.Sources = append(pbReq.Sources, &pb.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	pbResp, err := c.client.DevCheckSentMailBoxsToSource(ctx, pbReq, opts...)
	if err != nil {
		return
	}
	sent := make([]*model.SourceMailEvent, 0)
	notSent := make([]*model.SourceMailEvent, 0)
	for _, value := range pbResp.Sent {
		sent = append(sent, &model.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	for _, value := range pbResp.NotSent {
		notSent = append(notSent, &model.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	resp = &model.CheckSentMailBoxsToSourceResp{
		Message: pbResp.Message,
		Code:    pbResp.Code,
		Sent:    sent,
		NotSent: notSent,
	}
	return
}
func (c *MailboxInternalServices) ApiKeyCheckSentMailBoxsToSource(ctx context.Context, req *model.CheckSentMailBoxsToSourceReq, opts ...grpc.CallOption) (resp *model.CheckSentMailBoxsToSourceResp, err error) {
	pbReq := &pb.CheckSentMailBoxsToSourceReq{}
	pbReq.Sources = make([]*pb.SourceMailEvent, 0)
	for _, value := range req.Sources {
		pbReq.Sources = append(pbReq.Sources, &pb.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	pbResp, err := c.client.ApiKeyCheckSentMailBoxsToSource(ctx, pbReq, opts...)
	if err != nil {
		return
	}
	sent := make([]*model.SourceMailEvent, 0)
	notSent := make([]*model.SourceMailEvent, 0)
	for _, value := range pbResp.Sent {
		sent = append(sent, &model.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	for _, value := range pbResp.NotSent {
		notSent = append(notSent, &model.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	resp = &model.CheckSentMailBoxsToSourceResp{
		Message: pbResp.Message,
		Code:    pbResp.Code,
		Sent:    sent,
		NotSent: notSent,
	}
	return
}
func (c *MailboxInternalServices) InternalCheckSentMailBoxsToSource(ctx context.Context, req *model.CheckSentMailBoxsToSourceReq, opts ...grpc.CallOption) (resp *model.CheckSentMailBoxsToSourceResp, err error) {
	pbReq := &pb.CheckSentMailBoxsToSourceReq{}
	pbReq.Sources = make([]*pb.SourceMailEvent, 0)
	for _, value := range req.Sources {
		pbReq.Sources = append(pbReq.Sources, &pb.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	pbResp, err := c.client.InternalCheckSentMailBoxsToSource(ctx, pbReq, opts...)
	if err != nil {
		return
	}
	sent := make([]*model.SourceMailEvent, 0)
	notSent := make([]*model.SourceMailEvent, 0)
	for _, value := range pbResp.Sent {
		sent = append(sent, &model.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	for _, value := range pbResp.NotSent {
		notSent = append(notSent, &model.SourceMailEvent{
			SourceId:   value.SourceId,
			SourceType: value.SourceType,
			OwnerID:    value.OwnerID,
		})
	}
	resp = &model.CheckSentMailBoxsToSourceResp{
		Message: pbResp.Message,
		Code:    pbResp.Code,
		Sent:    sent,
		NotSent: notSent,
	}
	return
}
