// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.21.2
// source: avatar.proto

package pb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ReqAvatarItem struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	//id of avatar
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	//item id
	ItemId string `protobuf:"bytes,2,opt,name=itemId,proto3" json:"itemId,omitempty"`
	//type of avatar , ex: top, head, bot ..
	Type int32 `protobuf:"varint,3,opt,name=type,proto3" json:"type,omitempty"`
	//order of item in list
	Order int32 `protobuf:"varint,4,opt,name=order,proto3" json:"order,omitempty"`
}

func (x *ReqAvatarItem) Reset() {
	*x = ReqAvatarItem{}
	if protoimpl.UnsafeEnabled {
		mi := &file_avatar_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReqAvatarItem) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReqAvatarItem) ProtoMessage() {}

func (x *ReqAvatarItem) ProtoReflect() protoreflect.Message {
	mi := &file_avatar_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReqAvatarItem.ProtoReflect.Descriptor instead.
func (*ReqAvatarItem) Descriptor() ([]byte, []int) {
	return file_avatar_proto_rawDescGZIP(), []int{0}
}

func (x *ReqAvatarItem) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ReqAvatarItem) GetItemId() string {
	if x != nil {
		return x.ItemId
	}
	return ""
}

func (x *ReqAvatarItem) GetType() int32 {
	if x != nil {
		return x.Type
	}
	return 0
}

func (x *ReqAvatarItem) GetOrder() int32 {
	if x != nil {
		return x.Order
	}
	return 0
}

type UpdateAvatarReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Includes string `protobuf:"bytes,1,opt,name=includes,proto3" json:"includes,omitempty"`
	//list of item want update
	Items []*ReqAvatarItem `protobuf:"bytes,2,rep,name=items,proto3" json:"items,omitempty"`
	//name of avatar
	Name string `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	//profile id , if not set will get avatar of owner and client in token
	ProfileId string `protobuf:"bytes,4,opt,name=profileId,proto3" json:"profileId,omitempty"`
	//meta data of avatar
	Meta string `protobuf:"bytes,5,opt,name=meta,proto3" json:"meta,omitempty"`
}

func (x *UpdateAvatarReq) Reset() {
	*x = UpdateAvatarReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_avatar_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateAvatarReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateAvatarReq) ProtoMessage() {}

func (x *UpdateAvatarReq) ProtoReflect() protoreflect.Message {
	mi := &file_avatar_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateAvatarReq.ProtoReflect.Descriptor instead.
func (*UpdateAvatarReq) Descriptor() ([]byte, []int) {
	return file_avatar_proto_rawDescGZIP(), []int{1}
}

func (x *UpdateAvatarReq) GetIncludes() string {
	if x != nil {
		return x.Includes
	}
	return ""
}

func (x *UpdateAvatarReq) GetItems() []*ReqAvatarItem {
	if x != nil {
		return x.Items
	}
	return nil
}

func (x *UpdateAvatarReq) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *UpdateAvatarReq) GetProfileId() string {
	if x != nil {
		return x.ProfileId
	}
	return ""
}

func (x *UpdateAvatarReq) GetMeta() string {
	if x != nil {
		return x.Meta
	}
	return ""
}

type UpdateAvatarReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code int32 `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	//mesage info
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	//id of updated avatar
	Id string `protobuf:"bytes,3,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *UpdateAvatarReply) Reset() {
	*x = UpdateAvatarReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_avatar_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateAvatarReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateAvatarReply) ProtoMessage() {}

func (x *UpdateAvatarReply) ProtoReflect() protoreflect.Message {
	mi := &file_avatar_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateAvatarReply.ProtoReflect.Descriptor instead.
func (*UpdateAvatarReply) Descriptor() ([]byte, []int) {
	return file_avatar_proto_rawDescGZIP(), []int{2}
}

func (x *UpdateAvatarReply) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *UpdateAvatarReply) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *UpdateAvatarReply) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetAvatarReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	//avatar id
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	//owner id
	OwnerId string `protobuf:"bytes,2,opt,name=ownerId,proto3" json:"ownerId,omitempty"`
}

func (x *GetAvatarReq) Reset() {
	*x = GetAvatarReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_avatar_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAvatarReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAvatarReq) ProtoMessage() {}

func (x *GetAvatarReq) ProtoReflect() protoreflect.Message {
	mi := &file_avatar_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAvatarReq.ProtoReflect.Descriptor instead.
func (*GetAvatarReq) Descriptor() ([]byte, []int) {
	return file_avatar_proto_rawDescGZIP(), []int{3}
}

func (x *GetAvatarReq) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *GetAvatarReq) GetOwnerId() string {
	if x != nil {
		return x.OwnerId
	}
	return ""
}

type GetAvatarReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code int32 `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	//mesage info
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	//retrun name of avatar
	Name string `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	//retrun profile Id
	ProfileId string `protobuf:"bytes,4,opt,name=profileId,proto3" json:"profileId,omitempty"`
	//return meta data
	Meta string `protobuf:"bytes,5,opt,name=meta,proto3" json:"meta,omitempty"`
	//list of item in avatar set
	Items []*ReqAvatarItem `protobuf:"bytes,6,rep,name=items,proto3" json:"items,omitempty"`
	//return id avatar
	Id string `protobuf:"bytes,7,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetAvatarReply) Reset() {
	*x = GetAvatarReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_avatar_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAvatarReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAvatarReply) ProtoMessage() {}

func (x *GetAvatarReply) ProtoReflect() protoreflect.Message {
	mi := &file_avatar_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAvatarReply.ProtoReflect.Descriptor instead.
func (*GetAvatarReply) Descriptor() ([]byte, []int) {
	return file_avatar_proto_rawDescGZIP(), []int{4}
}

func (x *GetAvatarReply) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *GetAvatarReply) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *GetAvatarReply) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *GetAvatarReply) GetProfileId() string {
	if x != nil {
		return x.ProfileId
	}
	return ""
}

func (x *GetAvatarReply) GetMeta() string {
	if x != nil {
		return x.Meta
	}
	return ""
}

func (x *GetAvatarReply) GetItems() []*ReqAvatarItem {
	if x != nil {
		return x.Items
	}
	return nil
}

func (x *GetAvatarReply) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetAvatarByClientReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ClientId string `protobuf:"bytes,1,opt,name=clientId,proto3" json:"clientId,omitempty"`
}

func (x *GetAvatarByClientReq) Reset() {
	*x = GetAvatarByClientReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_avatar_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAvatarByClientReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAvatarByClientReq) ProtoMessage() {}

func (x *GetAvatarByClientReq) ProtoReflect() protoreflect.Message {
	mi := &file_avatar_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAvatarByClientReq.ProtoReflect.Descriptor instead.
func (*GetAvatarByClientReq) Descriptor() ([]byte, []int) {
	return file_avatar_proto_rawDescGZIP(), []int{5}
}

func (x *GetAvatarByClientReq) GetClientId() string {
	if x != nil {
		return x.ClientId
	}
	return ""
}

var File_avatar_proto protoreflect.FileDescriptor

var file_avatar_proto_rawDesc = []byte{
	0x0a, 0x0c, 0x61, 0x76, 0x61, 0x74, 0x61, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x02,
	0x70, 0x62, 0x22, 0x61, 0x0a, 0x0d, 0x52, 0x65, 0x71, 0x41, 0x76, 0x61, 0x74, 0x61, 0x72, 0x49,
	0x74, 0x65, 0x6d, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x69, 0x74, 0x65, 0x6d, 0x49, 0x64, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x69, 0x74, 0x65, 0x6d, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x74,
	0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12,
	0x14, 0x0a, 0x05, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x22, 0x9c, 0x01, 0x0a, 0x0f, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x41, 0x76, 0x61, 0x74, 0x61, 0x72, 0x52, 0x65, 0x71, 0x12, 0x1a, 0x0a, 0x08, 0x69, 0x6e, 0x63,
	0x6c, 0x75, 0x64, 0x65, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x69, 0x6e, 0x63,
	0x6c, 0x75, 0x64, 0x65, 0x73, 0x12, 0x27, 0x0a, 0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x02,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x11, 0x2e, 0x70, 0x62, 0x2e, 0x52, 0x65, 0x71, 0x41, 0x76, 0x61,
	0x74, 0x61, 0x72, 0x49, 0x74, 0x65, 0x6d, 0x52, 0x05, 0x69, 0x74, 0x65, 0x6d, 0x73, 0x12, 0x12,
	0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61,
	0x6d, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x64,
	0x12, 0x12, 0x0a, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04,
	0x6d, 0x65, 0x74, 0x61, 0x22, 0x51, 0x0a, 0x11, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x41, 0x76,
	0x61, 0x74, 0x61, 0x72, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64,
	0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a,
	0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x38, 0x0a, 0x0c, 0x47, 0x65, 0x74, 0x41, 0x76,
	0x61, 0x74, 0x61, 0x72, 0x52, 0x65, 0x71, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x6f, 0x77, 0x6e, 0x65, 0x72,
	0x49, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x77, 0x6e, 0x65, 0x72, 0x49,
	0x64, 0x22, 0xbd, 0x01, 0x0a, 0x0e, 0x47, 0x65, 0x74, 0x41, 0x76, 0x61, 0x74, 0x61, 0x72, 0x52,
	0x65, 0x70, 0x6c, 0x79, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73,
	0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61,
	0x67, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c,
	0x65, 0x49, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x66, 0x69,
	0x6c, 0x65, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x12, 0x27, 0x0a, 0x05, 0x69, 0x74, 0x65, 0x6d,
	0x73, 0x18, 0x06, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x11, 0x2e, 0x70, 0x62, 0x2e, 0x52, 0x65, 0x71,
	0x41, 0x76, 0x61, 0x74, 0x61, 0x72, 0x49, 0x74, 0x65, 0x6d, 0x52, 0x05, 0x69, 0x74, 0x65, 0x6d,
	0x73, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x22, 0x32, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x41, 0x76, 0x61, 0x74, 0x61, 0x72, 0x42, 0x79,
	0x43, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x12, 0x1a, 0x0a, 0x08, 0x63, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x63, 0x6c, 0x69,
	0x65, 0x6e, 0x74, 0x49, 0x64, 0x42, 0x0c, 0x5a, 0x0a, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65,
	0x2f, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_avatar_proto_rawDescOnce sync.Once
	file_avatar_proto_rawDescData = file_avatar_proto_rawDesc
)

func file_avatar_proto_rawDescGZIP() []byte {
	file_avatar_proto_rawDescOnce.Do(func() {
		file_avatar_proto_rawDescData = protoimpl.X.CompressGZIP(file_avatar_proto_rawDescData)
	})
	return file_avatar_proto_rawDescData
}

var file_avatar_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_avatar_proto_goTypes = []interface{}{
	(*ReqAvatarItem)(nil),        // 0: pb.ReqAvatarItem
	(*UpdateAvatarReq)(nil),      // 1: pb.UpdateAvatarReq
	(*UpdateAvatarReply)(nil),    // 2: pb.UpdateAvatarReply
	(*GetAvatarReq)(nil),         // 3: pb.GetAvatarReq
	(*GetAvatarReply)(nil),       // 4: pb.GetAvatarReply
	(*GetAvatarByClientReq)(nil), // 5: pb.GetAvatarByClientReq
}
var file_avatar_proto_depIdxs = []int32{
	0, // 0: pb.UpdateAvatarReq.items:type_name -> pb.ReqAvatarItem
	0, // 1: pb.GetAvatarReply.items:type_name -> pb.ReqAvatarItem
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_avatar_proto_init() }
func file_avatar_proto_init() {
	if File_avatar_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_avatar_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReqAvatarItem); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_avatar_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateAvatarReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_avatar_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateAvatarReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_avatar_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAvatarReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_avatar_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAvatarReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_avatar_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAvatarByClientReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_avatar_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_avatar_proto_goTypes,
		DependencyIndexes: file_avatar_proto_depIdxs,
		MessageInfos:      file_avatar_proto_msgTypes,
	}.Build()
	File_avatar_proto = out.File
	file_avatar_proto_rawDesc = nil
	file_avatar_proto_goTypes = nil
	file_avatar_proto_depIdxs = nil
}
