package leaderboard

import (
	"context"
	"errors"
	"log"
	"time"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/leaderboard/model"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/leaderboard"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type leaderboardInternalService struct {
	client pb.MimilandLeaderBoardServiceClient
}

func NewLeaderBoardInternalService(url string, expireRequest int64) *leaderboardInternalService {
	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) * time.Second // Set your desired timeout duration
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	var (
		cred = insecure.NewCredentials()
	)

	conn, err := grpc.DialContext(ctx, url, grpc.WithTransportCredentials(cred))
	if err != nil {
		log.Printf("dial grpc to leaderboard error: %v", err)
		return nil
	}
	log.Println("dial grpc to leaderboard successfully")
	return &leaderboardInternalService{
		client: pb.NewMimilandLeaderBoardServiceClient(conn),
	}
}

func (l *leaderboardInternalService) Ping(ctx context.Context) (message string, err error) {
	resp, err := l.client.Ping(ctx, &pb.LB_PingReq{})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	message = resp.Message
	return
}

func (l *leaderboardInternalService) GetLeaderBoard(ctx context.Context, req model.GetLeaderBoardParamModel) (data []*pb.LB_Data, err error) {
	resp, err := l.client.GetLeaderBoard(ctx, &pb.LB_GetReq{
		Top:     req.Top,
		OwnerId: req.OwnerId,
	})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	data = resp.Data
	return
}

func (l *leaderboardInternalService) GetLeaderBoardV2(ctx context.Context, req model.GetLeaderBoardParamModel) (data, aroundData []*pb.LB_Data, ownerData *pb.LB_Data, err error) {
	resp, err := l.client.GetLeaderBoardV2(ctx, &pb.LB_GetV2Req{
		Top:     req.Top,
		OwnerId: req.OwnerId,
		Around:  req.Around,
		Name:    req.Name,
	})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	data = resp.Data
	ownerData = resp.OwnerData
	aroundData = resp.AroundData
	return
}

func (l *leaderboardInternalService) GetLeaderBoardV3(ctx context.Context, req model.GetLeaderBoardParamModel) (data, aroundData []*pb.LB_Data, ownerData *pb.LB_Data, err error) {
	resp, err := l.client.GetLeaderBoardV3(ctx, &pb.LB_GetV3Req{
		Top:          req.Top,
		OwnerId:      req.OwnerId,
		Around:       req.Around,
		Name:         req.Name,
		IsGetHistory: req.IsGetHistory,
		HistoryData:  req.HistoryData,
	})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	data = resp.Data
	ownerData = resp.OwnerData
	aroundData = resp.AroundData
	return
}

func (l *leaderboardInternalService) ClearData(ctx context.Context, ownerId, clientId, name string) (success bool, err error) {
	resp, err := l.client.ClearData(ctx, &pb.LB_ClearDataReq{
		OwnerId:  ownerId,
		ClientId: clientId,
		Name:     name,
	})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	success = true
	return
}

func (l *leaderboardInternalService) ResetData(ctx context.Context) (success bool, err error) {
	resp, err := l.client.ResetData(ctx, &pb.LB_ResetDataReq{})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	success = true
	return
}
