package model

type GetProfile struct {
	Includes string `json:"includes"` // includes datas, accept: avatar|items
	Id       string `json:"id"`       // IdInWorld of user
	OwnerId  string `json:"ownerId"`  // UserId
}

type UpdateProfile struct {
	Includes string `json:"includes"` // includes datas: avatar|items
	Name     string `json:"name"`
	Gender   int32  `json:"gender"`
	Url      string `json:"url"`
	YoB      int32  `json:"yob"`
	ShareAge bool   `json:"shareAge"`
	Status   string `json:"status"`
	Active   bool   `json:"active"`
}

type UpdateAvatarItem struct {
	Id       string `json:"id"` // IdInWorld of user
	ItemId   string `json:"itemId"`
	Type     int32  `json:"type"`  // type of avatar, ex: top, head, bot, ...
	Order    int32  `json:"order"` // order of item in list
	DesignId string `json:"designId"`
}

type ListUpdateAvatars []*UpdateAvatarItem
