package model

type CreateMailsRequest struct {
	ClientId string       `json:"clientId,omitempty" bson:"clientId,omitempty"`
	UserIds  []string     `json:"userIds,omitempty" bson:"userIds,omitempty"`
	Mail     *MailBoxData `json:"mail,omitempty" bson:"mail,omitempty"`
}
type MailBoxData struct {
	Id          string           `json:"id,omitempty" bson:"id,omitempty"`
	From        string           `json:"from,omitempty" bson:"from,omitempty"`
	To          string           `json:"to,omitempty" bson:"to,omitempty"`
	Title       string           `json:"title,omitempty" bson:"title,omitempty"`
	Body        string           `json:"body,omitempty" bson:"body,omitempty"`
	CreatedTime int64            `json:"createdTime,omitempty" bson:"createdTime,omitempty"`
	ExpiredTime int64            `json:"expiredTime,omitempty" bson:"expiredTime,omitempty"`
	Attachments []*MailBoxAttach `json:"attachments,omitempty" bson:"attachments,omitempty"`
	Status      string           `json:"status,omitempty" bson:"status,omitempty"`
	ClientId    string           `json:"clientId,omitempty" bson:"clientId,omitempty"`
	TemplateUid string           `json:"templateUid,omitempty" bson:"templateUid,omitempty"`
	SourceId    string           `json:"sourceId,omitempty" bson:"sourceId,omitempty"`
	SourceType  string           `json:"sourceType,omitempty" bson:"sourceType,omitempty"`
	CreatedBy   string           `json:"createdBy,omitempty" bson:"createdBy,omitempty"`
}

type MailBoxAttach struct {
	Key      string  `json:"key,omitempty" bson:"key,omitempty"`
	Type     string  `json:"type,omitempty" bson:"type,omitempty"`
	Content  string  `json:"content,omitempty" bson:"content,omitempty"`
	Name     string  `json:"name,omitempty" bson:"name,omitempty"`
	Value    float32 `json:"value,omitempty" bson:"value,omitempty"`
	IsClaim  bool    `json:"isClaim,omitempty" bson:"isClaim,omitempty"`
	Uid      string  `json:"uid,omitempty" bson:"uid,omitempty"`
	SourceId string  `json:"sourceId,omitempty" bson:"sourceId,omitempty"`
}
type CheckSentMailBoxsToSourceReq struct {
	Sources []*SourceMailEvent `json:"sources,omitempty" bson:"sources,omitempty"`
}

type SourceMailEvent struct {
	SourceId   string `json:"sourceId,omitempty" bson:"sourceId,omitempty"`
	SourceType string `json:"sourceType,omitempty" bson:"sourceType,omitempty"`
	OwnerID    string `json:"ownerId,omitempty" bson:"ownerId,omitempty"`
}
type CreateMailsResponse struct {
	Code    int32  `json:"code,omitempty" bson:"code,omitempty"`
	Message string `json:"message,omitempty" bson:"message,omitempty"`
}
type CheckSentMailBoxsToSourceResp struct {
	Code    int32              `json:"code,omitempty" bson:"code,omitempty"`
	Message string             `json:"message,omitempty" bson:"message,omitempty"`
	Sent    []*SourceMailEvent `json:"sent,omitempty" bson:"sent,omitempty"`
	NotSent []*SourceMailEvent `json:"notSent,omitempty" bson:"notSent,omitempty"`
}
