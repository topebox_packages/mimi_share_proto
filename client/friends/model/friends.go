package model

type BecomeFriendRequest struct {
	OwnerId  string `json:"ownerId"`
	FriendId string `json:"friendId"`
}

type BecomeFriendResponse struct {
	Id   string `json:"id"`
	Code string `json:"code"`
}
type RemoveFriendParam struct {

	// id of friend you want remove
	FriendId string `protobuf:"bytes,1,opt,name=friendId,proto3" json:"friendId,omitempty"`
	// id in world of friend you want remove
	FriendIdInWorld string `protobuf:"bytes,2,opt,name=friendIdInWorld,proto3" json:"friendIdInWorld,omitempty"`
}
