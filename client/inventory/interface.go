package inventory

import (
	"context"

	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/inventory"
)

type IInventoryInternalService interface {
	Home(ctx context.Context) (code int32, message string, err error)
	Reward(ctx context.Context, designIds []string) (resp *pb.RewardItemsResp, err error)
	DevInitDesign(ctx context.Context, req *pb.DevInitDesignReq) (resp *pb.DevInitDesignResp, err error)
	DevPutDesign(ctx context.Context, req *pb.DevPutDesignReq) (resp *pb.DevPutDesignResp, err error)
	PutDesignAttributes(ctx context.Context, req *pb.PutDesignAttributesReq) (resp *pb.PutDesignAttributesResp, err error)
	DevBuildManifest(ctx context.Context, req *pb.DevBuildManifestReq) (resp *pb.DevBuildManifestResp, err error)
	DevTransferOwner(ctx context.Context, req *pb.DevTransferOwnerReq) (resp *pb.DevTransferOwnerResp, err error)
	AdminAddItems(ctx context.Context, req *pb.AdminAddItemsReq) (resp *pb.AdminAddItemsResp, err error)
	PutSharedItems(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error)
	ListDesign(ctx context.Context, req *pb.ListItemReqV2) (resp *pb.ListItemRespV2, err error)
	ListDesignV2(ctx context.Context, req *pb.ListItemReqV2) (resp *pb.ListItemRespV2, err error)
	ListItem(ctx context.Context, req *pb.ListItemReq) (resp *pb.ListItemResp, err error)
	GetItemByID(ctx context.Context, req *pb.GetItemByIDRequest) (resp *pb.GetItemByIDResponse, err error)
	ListItemV2(ctx context.Context, req *pb.ListItemReqV2) (resp *pb.ListItemRespV2, err error)
	ListItemV3(ctx context.Context, req *pb.ListItemReqV3) (resp *pb.ListItemRespV3, err error)
	RewardItems(ctx context.Context, req *pb.RewardItemsReq) (resp *pb.RewardItemsResp, err error)
	RewardItemsV2(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error)
	RewardPooledItemsV2(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error)
	FindAndRewardItems(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error)
	RandomItems(ctx context.Context, req *pb.RandomItemsReq) (resp *pb.RandomItemsResp, err error)
	SelectItems(ctx context.Context, req *pb.SelectItemsReq) (resp *pb.SelectItemsResp, err error)
	DeleteItems(ctx context.Context, req *pb.DeleteItemsReq) (resp *pb.DeleteItemsResp, err error)
	PutLevel(ctx context.Context, req *pb.PutLevelReq) (resp *pb.PutLevelResp, err error)
	UpgradeLevel(ctx context.Context, req *pb.UpgradeLevelReq) (resp *pb.UpgradeLevelResp, err error)
	UpgradeRarity(ctx context.Context, req *pb.UpgradeRarityReq) (resp *pb.UpgradeRarityResp, err error)
	UpdateMeta(ctx context.Context, req *pb.UpdateMetaReq) (resp *pb.UpdateMetaResp, err error)
	PutAttributes(ctx context.Context, req *pb.PutAttributesReq) (resp *pb.PutAttributesResp, err error)
	ImportItems(ctx context.Context, req *pb.ImportItemsReq) (resp *pb.ImportItemsResp, err error)
	UpdateMainFields(ctx context.Context, req *pb.UpdateMainFieldsReq) (resp *pb.UpdateMainFieldsResp, err error)
	UpgradeExp(ctx context.Context, req *pb.UpgradeExpReq) (resp *pb.UpgradeExpResp, err error)
	GetPoolItems(ctx context.Context, req *pb.GetPoolItemsReq) (resp *pb.GetPoolItemsResp, err error)
	InsertItemDesign(ctx context.Context, req *pb.UploadItemDesignPub) (resp *pb.Empty, err error)
	RefreshDesignModel(ctx context.Context, req *pb.RefreshDesignModelReq) (resp *pb.RefreshDesignModelResp, err error)
	GetTotalItems(ctx context.Context) (total int64, err error)
	GetListSharedItems(ctx context.Context, req *pb.GetListSharedItemsRequest) (resp *pb.GetListSharedItemsResponse, err error)
	GetExpiredSharedItems(ctx context.Context, req *pb.GetExpiredSharedItemsRequest) (resp *pb.GetExpiredSharedItemsResponse, err error)
	RewardSharedItems(ctx context.Context, req *pb.RewardSharedItemsReq) (resp *pb.RewardSharedItemsResp, err error)
}
