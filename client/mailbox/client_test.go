package mailbox_test

import (
	"context"
	"strings"
	"testing"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/mailbox"
	pbmailbox "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/mailbox"
	"go.nhat.io/grpcmock"
)

func TestMailboxServiceClient_Ping(t *testing.T) {
	srv := grpcmock.MockServer(
		grpcmock.RegisterService(pbmailbox.RegisterMailboxServiceServer),
		func(s *grpcmock.Server) {
			s.ExpectUnary("pb.MailboxService/Ping").
				Return(&pbmailbox.MB_PingResp{
					Message: "pong",
					Code:    200,
				})
		},
	)(grpcmock.NoOpT())
	client := mailbox.GetMailboxInternalService(srv.Address(), 0)
	if client == nil {
		t.Error("NewMailboxClientInternalService() error")
		return
	}
	message, err := client.Ping(context.Background())
	if err != nil {
		t.Errorf("Ping() error = %v", err)
		return
	}
	message = strings.TrimSpace(message)
	if message != "pong" {
		t.Errorf("Ping() message = %v", message)
		return
	}
}
