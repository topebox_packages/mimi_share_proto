package profile_test

import (
	"context"
	"log"
	"strings"
	"testing"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/profile"
	pbprofile "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/profile"
	"go.nhat.io/grpcmock"
	"google.golang.org/grpc/metadata"
)

func TestProfileServiceClient_Ping(t *testing.T) {
	srv := grpcmock.MockServer(
		grpcmock.RegisterService(pbprofile.RegisterProfileServiceServer),
		func(s *grpcmock.Server) {
			s.ExpectUnary("pb.ProfileService/Ping").
				Return(&pbprofile.PingProfileResp{
					Message: "pong",
					Code:    200,
				})
		},
	)(grpcmock.NoOpT())
	client := profile.NewProfileClientInternalService(srv.Address(), 0)
	if client == nil {
		t.Error("NewProfileClientInternalService() error")
		return
	}
	message, err := client.Ping(context.Background())
	if err != nil {
		t.Errorf("Ping() error = %v", err)
		return
	}
	message = strings.TrimSpace(message)
	if message != "pong" {
		t.Errorf("Ping() message = %v", message)
		return
	}
}

func TestProfileServiceClient_GetProfileByOwnerId(t *testing.T) {
	host := "0.0.0.0:9020" // replace this host to profile grpc host
	idInWorld := "59541f3b"
	apiKey := "F34F95B50D017127B118966CF7CE3786600DC81059DBEE0348134CD476067D2E"
	client := profile.NewProfileClientInternalService(host, 0)
	if client == nil {
		t.Error("NewProfileClientInternalService() error")
		return
	}
	ctx := context.Background()

	md := metadata.Pairs(
		"mm-api-key", apiKey,
		"mm-client-id", "mml",
	)
	ctx = metadata.NewOutgoingContext(ctx, md)
	result, err := client.GetProfileByIdInWorld(ctx, idInWorld)
	if err != nil {
		t.Error(err)
		return
	}
	log.Println(&result)
	if result != nil && result.Code == 200 && result.OwnerIdInWorld == idInWorld {
		log.Println("Success")
	} else {
		t.Error("GetProfileByIdInWorld error")
	}
}
