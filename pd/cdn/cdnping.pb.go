// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.21.9
// source: cdnping.proto

package cdn

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CDNPingReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *CDNPingReq) Reset() {
	*x = CDNPingReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CDNPingReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CDNPingReq) ProtoMessage() {}

func (x *CDNPingReq) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CDNPingReq.ProtoReflect.Descriptor instead.
func (*CDNPingReq) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{0}
}

type CDNPingReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32  `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *CDNPingReply) Reset() {
	*x = CDNPingReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CDNPingReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CDNPingReply) ProtoMessage() {}

func (x *CDNPingReply) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CDNPingReply.ProtoReflect.Descriptor instead.
func (*CDNPingReply) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{1}
}

func (x *CDNPingReply) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *CDNPingReply) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type CDNIndexReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *CDNIndexReq) Reset() {
	*x = CDNIndexReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CDNIndexReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CDNIndexReq) ProtoMessage() {}

func (x *CDNIndexReq) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CDNIndexReq.ProtoReflect.Descriptor instead.
func (*CDNIndexReq) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{2}
}

type CDNIndexReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name    string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Version string `protobuf:"bytes,2,opt,name=version,proto3" json:"version,omitempty"`
	Author  string `protobuf:"bytes,3,opt,name=author,proto3" json:"author,omitempty"`
}

func (x *CDNIndexReply) Reset() {
	*x = CDNIndexReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CDNIndexReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CDNIndexReply) ProtoMessage() {}

func (x *CDNIndexReply) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CDNIndexReply.ProtoReflect.Descriptor instead.
func (*CDNIndexReply) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{3}
}

func (x *CDNIndexReply) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CDNIndexReply) GetVersion() string {
	if x != nil {
		return x.Version
	}
	return ""
}

func (x *CDNIndexReply) GetAuthor() string {
	if x != nil {
		return x.Author
	}
	return ""
}

type GetCDNTimeServerReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *GetCDNTimeServerReq) Reset() {
	*x = GetCDNTimeServerReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetCDNTimeServerReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetCDNTimeServerReq) ProtoMessage() {}

func (x *GetCDNTimeServerReq) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetCDNTimeServerReq.ProtoReflect.Descriptor instead.
func (*GetCDNTimeServerReq) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{4}
}

type GetCDNTimeServerReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TimeString string `protobuf:"bytes,1,opt,name=TimeString,proto3" json:"TimeString,omitempty"`
	UnixTime   int64  `protobuf:"varint,2,opt,name=UnixTime,proto3" json:"UnixTime,omitempty"`
	Code       int32  `protobuf:"varint,3,opt,name=code,proto3" json:"code,omitempty"`
	Message    string `protobuf:"bytes,4,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *GetCDNTimeServerReply) Reset() {
	*x = GetCDNTimeServerReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetCDNTimeServerReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetCDNTimeServerReply) ProtoMessage() {}

func (x *GetCDNTimeServerReply) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetCDNTimeServerReply.ProtoReflect.Descriptor instead.
func (*GetCDNTimeServerReply) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{5}
}

func (x *GetCDNTimeServerReply) GetTimeString() string {
	if x != nil {
		return x.TimeString
	}
	return ""
}

func (x *GetCDNTimeServerReply) GetUnixTime() int64 {
	if x != nil {
		return x.UnixTime
	}
	return 0
}

func (x *GetCDNTimeServerReply) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *GetCDNTimeServerReply) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type CDNBaseReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32  `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *CDNBaseReply) Reset() {
	*x = CDNBaseReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_cdnping_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CDNBaseReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CDNBaseReply) ProtoMessage() {}

func (x *CDNBaseReply) ProtoReflect() protoreflect.Message {
	mi := &file_cdnping_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CDNBaseReply.ProtoReflect.Descriptor instead.
func (*CDNBaseReply) Descriptor() ([]byte, []int) {
	return file_cdnping_proto_rawDescGZIP(), []int{6}
}

func (x *CDNBaseReply) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *CDNBaseReply) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

var File_cdnping_proto protoreflect.FileDescriptor

var file_cdnping_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x63, 0x64, 0x6e, 0x70, 0x69, 0x6e, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x03, 0x63, 0x64, 0x6e, 0x22, 0x0c, 0x0a, 0x0a, 0x43, 0x44, 0x4e, 0x50, 0x69, 0x6e, 0x67, 0x52,
	0x65, 0x71, 0x22, 0x3c, 0x0a, 0x0c, 0x43, 0x44, 0x4e, 0x50, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x70,
	0x6c, 0x79, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x22, 0x0d, 0x0a, 0x0b, 0x43, 0x44, 0x4e, 0x49, 0x6e, 0x64, 0x65, 0x78, 0x52, 0x65, 0x71, 0x22,
	0x55, 0x0a, 0x0d, 0x43, 0x44, 0x4e, 0x49, 0x6e, 0x64, 0x65, 0x78, 0x52, 0x65, 0x70, 0x6c, 0x79,
	0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6f, 0x6e, 0x12, 0x16,
	0x0a, 0x06, 0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x22, 0x15, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x43, 0x44, 0x4e,
	0x54, 0x69, 0x6d, 0x65, 0x53, 0x65, 0x72, 0x76, 0x65, 0x72, 0x52, 0x65, 0x71, 0x22, 0x81, 0x01,
	0x0a, 0x15, 0x47, 0x65, 0x74, 0x43, 0x44, 0x4e, 0x54, 0x69, 0x6d, 0x65, 0x53, 0x65, 0x72, 0x76,
	0x65, 0x72, 0x52, 0x65, 0x70, 0x6c, 0x79, 0x12, 0x1e, 0x0a, 0x0a, 0x54, 0x69, 0x6d, 0x65, 0x53,
	0x74, 0x72, 0x69, 0x6e, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x54, 0x69, 0x6d,
	0x65, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x12, 0x1a, 0x0a, 0x08, 0x55, 0x6e, 0x69, 0x78, 0x54,
	0x69, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x55, 0x6e, 0x69, 0x78, 0x54,
	0x69, 0x6d, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61,
	0x67, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x65, 0x22, 0x3c, 0x0a, 0x0c, 0x43, 0x44, 0x4e, 0x42, 0x61, 0x73, 0x65, 0x52, 0x65, 0x70, 0x6c,
	0x79, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52,
	0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x42,
	0x3b, 0x5a, 0x39, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x74, 0x6f,
	0x70, 0x65, 0x62, 0x6f, 0x78, 0x5f, 0x70, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x73, 0x2f, 0x6d,
	0x69, 0x6d, 0x69, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x63, 0x6c, 0x69, 0x65, 0x6e,
	0x74, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x70, 0x62, 0x2f, 0x63, 0x64, 0x6e, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_cdnping_proto_rawDescOnce sync.Once
	file_cdnping_proto_rawDescData = file_cdnping_proto_rawDesc
)

func file_cdnping_proto_rawDescGZIP() []byte {
	file_cdnping_proto_rawDescOnce.Do(func() {
		file_cdnping_proto_rawDescData = protoimpl.X.CompressGZIP(file_cdnping_proto_rawDescData)
	})
	return file_cdnping_proto_rawDescData
}

var file_cdnping_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_cdnping_proto_goTypes = []interface{}{
	(*CDNPingReq)(nil),            // 0: cdn.CDNPingReq
	(*CDNPingReply)(nil),          // 1: cdn.CDNPingReply
	(*CDNIndexReq)(nil),           // 2: cdn.CDNIndexReq
	(*CDNIndexReply)(nil),         // 3: cdn.CDNIndexReply
	(*GetCDNTimeServerReq)(nil),   // 4: cdn.GetCDNTimeServerReq
	(*GetCDNTimeServerReply)(nil), // 5: cdn.GetCDNTimeServerReply
	(*CDNBaseReply)(nil),          // 6: cdn.CDNBaseReply
}
var file_cdnping_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_cdnping_proto_init() }
func file_cdnping_proto_init() {
	if File_cdnping_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_cdnping_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CDNPingReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_cdnping_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CDNPingReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_cdnping_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CDNIndexReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_cdnping_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CDNIndexReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_cdnping_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetCDNTimeServerReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_cdnping_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetCDNTimeServerReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_cdnping_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CDNBaseReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_cdnping_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_cdnping_proto_goTypes,
		DependencyIndexes: file_cdnping_proto_depIdxs,
		MessageInfos:      file_cdnping_proto_msgTypes,
	}.Build()
	File_cdnping_proto = out.File
	file_cdnping_proto_rawDesc = nil
	file_cdnping_proto_goTypes = nil
	file_cdnping_proto_depIdxs = nil
}
