package profile

import (
	"context"
	"log"
	"time"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/utils"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/auth"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type AuthInternalServices struct {
	IAuthInternalService
	client       pb.MimilandAuthServiceClient
	configClient utils.InitClientParams
}

func GetAuthInternalService(url string, expireRequest int64) *AuthInternalServices {
	var profileSvc *AuthInternalServices

	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest)
	profileSvc = GetAuthInternalServiceWithParams(utils.InitClientParams{
		Url:          url,
		ExpireSecond: int64(timeout),
	})
	log.Println("dial to mimiland auth internal service successfully")
	return profileSvc
}

func GetAuthInternalServiceWithParams(params utils.InitClientParams, opts ...grpc.DialOption) *AuthInternalServices {
	var authSvc *AuthInternalServices

	if params.ExpireSecond == 0 {
		params.ExpireSecond = 120
	}

	// Create a context with the specified timeout
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(params.ExpireSecond)*time.Second)
	defer cancel()
	cred := insecure.NewCredentials()
	opts = append(opts, grpc.WithTransportCredentials(cred))
	retriableErrors := utils.DefaultRetriableErrors
	if len(params.RetriableErrors) > 0 {
		retriableErrors = append(retriableErrors, params.RetriableErrors...)
	}
	if params.RetryTime != 0 {
		unaryInterceptor := grpc_retry.UnaryClientInterceptor(grpc_retry.WithCodes(retriableErrors...), grpc_retry.WithMax(params.RetryTime), grpc_retry.WithPerRetryTimeout(params.PerRetryTimeout))
		opts = append(opts, grpc.WithUnaryInterceptor(unaryInterceptor))
	}
	conn, err := grpc.DialContext(ctx, params.Url, opts...)
	if err != nil {
		log.Println("cannot connect grpc authSvc service")
		return nil
	}
	authSvc = &AuthInternalServices{
		client: pb.NewMimilandAuthServiceClient(conn),
		configClient: utils.InitClientParams{
			Url:          params.Url,
			ExpireSecond: params.ExpireSecond,
		},
	}
	log.Println("dial to mimiland auth internal service successfully")

	return authSvc
}

func (p *AuthInternalServices) Ping(ctx context.Context) (resp *pb.AuthPingReply, err error) {
	resp, err = p.client.Ping(ctx, &pb.AuthPingReq{})
	return
}
func (p *AuthInternalServices) RevokeAppleId(ctx context.Context, req *pb.RevokeAppleTokenReq) (resp *pb.RevokeAppleTokenReply, err error) {
	resp, err = p.client.RevokeAppleId(ctx, req)
	return
}
func (p *AuthInternalServices) UnLinkAccount(ctx context.Context, req *pb.AccountLinkReq) (resp *pb.AccountLinkReply, err error) {
	resp, err = p.client.UnLinkAccount(ctx, req)
	return
}
func (p *AuthInternalServices) GetSession(ctx context.Context, req *pb.GetSessionReq) (resp *pb.GetSessionReply, err error) {
	resp, err = p.client.GetSession(ctx, req)
	return
}
func (p *AuthInternalServices) GetSessionV2(ctx context.Context, req *pb.GetSessionReq) (resp *pb.GetSessionV2Reply, err error) {
	resp, err = p.client.GetSessionV2(ctx, req)
	return
}
func (p *AuthInternalServices) GetUserInfo(ctx context.Context, req *pb.GetUserInfoReq) (resp *pb.GetUserInfoReply, err error) {
	resp, err = p.client.GetUserInfo(ctx, req)
	return
}
func (p *AuthInternalServices) GetUserInfoV2(ctx context.Context, req *pb.GetUserInfoReq) (resp *pb.GetUserInfoV2Reply, err error) {
	resp, err = p.client.GetUserInfoV2(ctx, req)
	return
}
