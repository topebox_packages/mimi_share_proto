package model

type GetLeaderBoardParamModel struct {
	Top          int32
	OwnerId      string
	Name         string
	Around       int32
	IsGetHistory bool
	HistoryData  map[string]string
}
