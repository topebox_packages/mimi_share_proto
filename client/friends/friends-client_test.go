package friends

import (
	"context"
	"log"
	"testing"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/friends/model"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/friends"
	"go.nhat.io/grpcmock"
)

func TestFriendsServiceClient_Ping(t *testing.T) {
	srv := grpcmock.MockServer(
		grpcmock.RegisterService(pb.RegisterMimilandServiceServer),
		func(s *grpcmock.Server) {
			s.ExpectUnary("friendpb.MimilandService/Ping").
				Return(&pb.PingReply{
					Message: "pong",
					Code:    200,
				})
		},
	)(grpcmock.NoOpT())
	log.Println(srv.Address())
	client := GetFriendsInternalService(srv.Address(), 0)
	if client == nil {
		t.Error("NewFriendsServiceClient() error")
		return
	}
	message, err := client.Ping(context.Background())
	if err != nil {
		t.Errorf("Ping() error = %v", err)
		return
	}
	if message != "pong" {
		t.Errorf("Ping() message = %v", message)
		return
	}
}

func TestFriendsServiceClient_BecomeFriend(t *testing.T) {
	srv := grpcmock.MockServer(
		grpcmock.RegisterService(pb.RegisterMimilandServiceServer),
		func(s *grpcmock.Server) {
			s.ExpectUnary("friendpb.MimilandService/BecomeFriend").
				Return(&pb.BecomeFriendReply{
					Code:    200,
					Message: "success",
					Id:      "123",
				})
		},
	)(grpcmock.NoOpT())
	log.Println(srv.Address())
	client := GetFriendsInternalService(srv.Address(), 0)
	if client == nil {
		t.Error("NewFriendsServiceClient() error")
		return
	}
	resp, err := client.BecomeFriend(context.Background(), &model.BecomeFriendRequest{
		OwnerId:  "123",
		FriendId: "456",
	})
	if err != nil {
		t.Errorf("BecomeFriend() error = %v", err)
		return
	}
	if resp.Id != "123" {
		t.Errorf("BecomeFriend() message id not %v", resp.Id)
		return
	}
}
