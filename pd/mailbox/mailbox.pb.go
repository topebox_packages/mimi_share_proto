// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.21.9
// source: mailbox.proto

package mailbox

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CreateMailsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32  `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *CreateMailsResponse) Reset() {
	*x = CreateMailsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateMailsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateMailsResponse) ProtoMessage() {}

func (x *CreateMailsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateMailsResponse.ProtoReflect.Descriptor instead.
func (*CreateMailsResponse) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{0}
}

func (x *CreateMailsResponse) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *CreateMailsResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type CreateMailsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ClientId string       `protobuf:"bytes,1,opt,name=clientId,proto3" json:"clientId,omitempty"`
	UserIds  []string     `protobuf:"bytes,2,rep,name=userIds,proto3" json:"userIds,omitempty"`
	Mail     *MailBoxData `protobuf:"bytes,3,opt,name=mail,proto3" json:"mail,omitempty"`
}

func (x *CreateMailsRequest) Reset() {
	*x = CreateMailsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateMailsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateMailsRequest) ProtoMessage() {}

func (x *CreateMailsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateMailsRequest.ProtoReflect.Descriptor instead.
func (*CreateMailsRequest) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{1}
}

func (x *CreateMailsRequest) GetClientId() string {
	if x != nil {
		return x.ClientId
	}
	return ""
}

func (x *CreateMailsRequest) GetUserIds() []string {
	if x != nil {
		return x.UserIds
	}
	return nil
}

func (x *CreateMailsRequest) GetMail() *MailBoxData {
	if x != nil {
		return x.Mail
	}
	return nil
}

type CheckSentMailBoxsToSourceReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Sources []*SourceMailEvent `protobuf:"bytes,1,rep,name=sources,proto3" json:"sources,omitempty"`
}

func (x *CheckSentMailBoxsToSourceReq) Reset() {
	*x = CheckSentMailBoxsToSourceReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CheckSentMailBoxsToSourceReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CheckSentMailBoxsToSourceReq) ProtoMessage() {}

func (x *CheckSentMailBoxsToSourceReq) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CheckSentMailBoxsToSourceReq.ProtoReflect.Descriptor instead.
func (*CheckSentMailBoxsToSourceReq) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{2}
}

func (x *CheckSentMailBoxsToSourceReq) GetSources() []*SourceMailEvent {
	if x != nil {
		return x.Sources
	}
	return nil
}

type SourceMailEvent struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SourceId   string `protobuf:"bytes,1,opt,name=sourceId,proto3" json:"sourceId,omitempty"`
	SourceType string `protobuf:"bytes,2,opt,name=sourceType,proto3" json:"sourceType,omitempty"`
	OwnerID    string `protobuf:"bytes,3,opt,name=ownerID,proto3" json:"ownerID,omitempty"`
}

func (x *SourceMailEvent) Reset() {
	*x = SourceMailEvent{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SourceMailEvent) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SourceMailEvent) ProtoMessage() {}

func (x *SourceMailEvent) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SourceMailEvent.ProtoReflect.Descriptor instead.
func (*SourceMailEvent) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{3}
}

func (x *SourceMailEvent) GetSourceId() string {
	if x != nil {
		return x.SourceId
	}
	return ""
}

func (x *SourceMailEvent) GetSourceType() string {
	if x != nil {
		return x.SourceType
	}
	return ""
}

func (x *SourceMailEvent) GetOwnerID() string {
	if x != nil {
		return x.OwnerID
	}
	return ""
}

type CheckSentMailBoxsToSourceResp struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32              `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string             `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	Sent    []*SourceMailEvent `protobuf:"bytes,3,rep,name=sent,proto3" json:"sent,omitempty"`
	NotSent []*SourceMailEvent `protobuf:"bytes,4,rep,name=notSent,proto3" json:"notSent,omitempty"`
}

func (x *CheckSentMailBoxsToSourceResp) Reset() {
	*x = CheckSentMailBoxsToSourceResp{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CheckSentMailBoxsToSourceResp) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CheckSentMailBoxsToSourceResp) ProtoMessage() {}

func (x *CheckSentMailBoxsToSourceResp) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CheckSentMailBoxsToSourceResp.ProtoReflect.Descriptor instead.
func (*CheckSentMailBoxsToSourceResp) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{4}
}

func (x *CheckSentMailBoxsToSourceResp) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *CheckSentMailBoxsToSourceResp) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *CheckSentMailBoxsToSourceResp) GetSent() []*SourceMailEvent {
	if x != nil {
		return x.Sent
	}
	return nil
}

func (x *CheckSentMailBoxsToSourceResp) GetNotSent() []*SourceMailEvent {
	if x != nil {
		return x.NotSent
	}
	return nil
}

type MailBoxAttach struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Key      string  `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	Type     string  `protobuf:"bytes,2,opt,name=type,proto3" json:"type,omitempty"`
	Content  string  `protobuf:"bytes,3,opt,name=content,proto3" json:"content,omitempty"`
	Name     string  `protobuf:"bytes,4,opt,name=name,proto3" json:"name,omitempty"`
	Value    float32 `protobuf:"fixed32,5,opt,name=value,proto3" json:"value,omitempty"`
	IsClaim  bool    `protobuf:"varint,6,opt,name=isClaim,proto3" json:"isClaim,omitempty"`
	Uid      string  `protobuf:"bytes,7,opt,name=uid,proto3" json:"uid,omitempty"`
	SourceId string  `protobuf:"bytes,8,opt,name=sourceId,proto3" json:"sourceId,omitempty"`
}

func (x *MailBoxAttach) Reset() {
	*x = MailBoxAttach{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MailBoxAttach) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MailBoxAttach) ProtoMessage() {}

func (x *MailBoxAttach) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MailBoxAttach.ProtoReflect.Descriptor instead.
func (*MailBoxAttach) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{5}
}

func (x *MailBoxAttach) GetKey() string {
	if x != nil {
		return x.Key
	}
	return ""
}

func (x *MailBoxAttach) GetType() string {
	if x != nil {
		return x.Type
	}
	return ""
}

func (x *MailBoxAttach) GetContent() string {
	if x != nil {
		return x.Content
	}
	return ""
}

func (x *MailBoxAttach) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *MailBoxAttach) GetValue() float32 {
	if x != nil {
		return x.Value
	}
	return 0
}

func (x *MailBoxAttach) GetIsClaim() bool {
	if x != nil {
		return x.IsClaim
	}
	return false
}

func (x *MailBoxAttach) GetUid() string {
	if x != nil {
		return x.Uid
	}
	return ""
}

func (x *MailBoxAttach) GetSourceId() string {
	if x != nil {
		return x.SourceId
	}
	return ""
}

type MailBoxData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string           `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	From        string           `protobuf:"bytes,2,opt,name=from,proto3" json:"from,omitempty"`
	To          string           `protobuf:"bytes,3,opt,name=to,proto3" json:"to,omitempty"`
	Title       string           `protobuf:"bytes,4,opt,name=title,proto3" json:"title,omitempty"`
	Body        string           `protobuf:"bytes,5,opt,name=body,proto3" json:"body,omitempty"`
	CreatedTime int64            `protobuf:"varint,6,opt,name=createdTime,proto3" json:"createdTime,omitempty"`
	ExpiredTime int64            `protobuf:"varint,7,opt,name=expiredTime,proto3" json:"expiredTime,omitempty"`
	Attachments []*MailBoxAttach `protobuf:"bytes,8,rep,name=attachments,proto3" json:"attachments,omitempty"`
	Status      string           `protobuf:"bytes,9,opt,name=status,proto3" json:"status,omitempty"`
	ClientId    string           `protobuf:"bytes,10,opt,name=clientId,proto3" json:"clientId,omitempty"`
	TemplateUid string           `protobuf:"bytes,11,opt,name=templateUid,proto3" json:"templateUid,omitempty"`
	FromProfile *ProfileInfo     `protobuf:"bytes,12,opt,name=fromProfile,proto3" json:"fromProfile,omitempty"`
	ToProfile   *ProfileInfo     `protobuf:"bytes,13,opt,name=toProfile,proto3" json:"toProfile,omitempty"`
	SourceId    string           `protobuf:"bytes,14,opt,name=sourceId,proto3" json:"sourceId,omitempty"`
	SourceType  string           `protobuf:"bytes,15,opt,name=sourceType,proto3" json:"sourceType,omitempty"`
	FromOwnerId string           `protobuf:"bytes,16,opt,name=fromOwnerId,proto3" json:"fromOwnerId,omitempty"`
	ToOwnerId   string           `protobuf:"bytes,17,opt,name=toOwnerId,proto3" json:"toOwnerId,omitempty"`
	ReadAt      int64            `protobuf:"varint,18,opt,name=readAt,proto3" json:"readAt,omitempty"`
	ReadTimes   int32            `protobuf:"varint,19,opt,name=readTimes,proto3" json:"readTimes,omitempty"`
	CreatedBy   string           `protobuf:"bytes,20,opt,name=createdBy,proto3" json:"createdBy,omitempty"`
}

func (x *MailBoxData) Reset() {
	*x = MailBoxData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *MailBoxData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*MailBoxData) ProtoMessage() {}

func (x *MailBoxData) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use MailBoxData.ProtoReflect.Descriptor instead.
func (*MailBoxData) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{6}
}

func (x *MailBoxData) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *MailBoxData) GetFrom() string {
	if x != nil {
		return x.From
	}
	return ""
}

func (x *MailBoxData) GetTo() string {
	if x != nil {
		return x.To
	}
	return ""
}

func (x *MailBoxData) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *MailBoxData) GetBody() string {
	if x != nil {
		return x.Body
	}
	return ""
}

func (x *MailBoxData) GetCreatedTime() int64 {
	if x != nil {
		return x.CreatedTime
	}
	return 0
}

func (x *MailBoxData) GetExpiredTime() int64 {
	if x != nil {
		return x.ExpiredTime
	}
	return 0
}

func (x *MailBoxData) GetAttachments() []*MailBoxAttach {
	if x != nil {
		return x.Attachments
	}
	return nil
}

func (x *MailBoxData) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *MailBoxData) GetClientId() string {
	if x != nil {
		return x.ClientId
	}
	return ""
}

func (x *MailBoxData) GetTemplateUid() string {
	if x != nil {
		return x.TemplateUid
	}
	return ""
}

func (x *MailBoxData) GetFromProfile() *ProfileInfo {
	if x != nil {
		return x.FromProfile
	}
	return nil
}

func (x *MailBoxData) GetToProfile() *ProfileInfo {
	if x != nil {
		return x.ToProfile
	}
	return nil
}

func (x *MailBoxData) GetSourceId() string {
	if x != nil {
		return x.SourceId
	}
	return ""
}

func (x *MailBoxData) GetSourceType() string {
	if x != nil {
		return x.SourceType
	}
	return ""
}

func (x *MailBoxData) GetFromOwnerId() string {
	if x != nil {
		return x.FromOwnerId
	}
	return ""
}

func (x *MailBoxData) GetToOwnerId() string {
	if x != nil {
		return x.ToOwnerId
	}
	return ""
}

func (x *MailBoxData) GetReadAt() int64 {
	if x != nil {
		return x.ReadAt
	}
	return 0
}

func (x *MailBoxData) GetReadTimes() int32 {
	if x != nil {
		return x.ReadTimes
	}
	return 0
}

func (x *MailBoxData) GetCreatedBy() string {
	if x != nil {
		return x.CreatedBy
	}
	return ""
}

type ProfileInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// id of document
	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	// name of profile
	Name string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	// status of profile
	Status string `protobuf:"bytes,3,opt,name=status,proto3" json:"status,omitempty"`
	// url of profile
	Url string `protobuf:"bytes,4,opt,name=url,proto3" json:"url,omitempty"`
	// gender of profile
	Gender int32 `protobuf:"varint,5,opt,name=gender,proto3" json:"gender,omitempty"`
	Active bool  `protobuf:"varint,6,opt,name=active,proto3" json:"active,omitempty"`
	// owner id want get info
	OwnerId string `protobuf:"bytes,7,opt,name=ownerId,proto3" json:"ownerId,omitempty"`
	// owner id want get info
	OwnerIdInWorld string `protobuf:"bytes,8,opt,name=ownerIdInWorld,proto3" json:"ownerIdInWorld,omitempty"`
	YoB            int32  `protobuf:"varint,9,opt,name=YoB,proto3" json:"YoB,omitempty"`
	ShareAge       bool   `protobuf:"varint,10,opt,name=ShareAge,proto3" json:"ShareAge,omitempty"`
}

func (x *ProfileInfo) Reset() {
	*x = ProfileInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_mailbox_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProfileInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProfileInfo) ProtoMessage() {}

func (x *ProfileInfo) ProtoReflect() protoreflect.Message {
	mi := &file_mailbox_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProfileInfo.ProtoReflect.Descriptor instead.
func (*ProfileInfo) Descriptor() ([]byte, []int) {
	return file_mailbox_proto_rawDescGZIP(), []int{7}
}

func (x *ProfileInfo) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ProfileInfo) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *ProfileInfo) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *ProfileInfo) GetUrl() string {
	if x != nil {
		return x.Url
	}
	return ""
}

func (x *ProfileInfo) GetGender() int32 {
	if x != nil {
		return x.Gender
	}
	return 0
}

func (x *ProfileInfo) GetActive() bool {
	if x != nil {
		return x.Active
	}
	return false
}

func (x *ProfileInfo) GetOwnerId() string {
	if x != nil {
		return x.OwnerId
	}
	return ""
}

func (x *ProfileInfo) GetOwnerIdInWorld() string {
	if x != nil {
		return x.OwnerIdInWorld
	}
	return ""
}

func (x *ProfileInfo) GetYoB() int32 {
	if x != nil {
		return x.YoB
	}
	return 0
}

func (x *ProfileInfo) GetShareAge() bool {
	if x != nil {
		return x.ShareAge
	}
	return false
}

var File_mailbox_proto protoreflect.FileDescriptor

var file_mailbox_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x6d, 0x61, 0x69, 0x6c, 0x62, 0x6f, 0x78, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x02, 0x70, 0x62, 0x22, 0x43, 0x0a, 0x13, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x4d, 0x61, 0x69,
	0x6c, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18,
	0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x22, 0x6f, 0x0a, 0x12, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x4d, 0x61, 0x69, 0x6c, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1a,
	0x0a, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x75, 0x73,
	0x65, 0x72, 0x49, 0x64, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x09, 0x52, 0x07, 0x75, 0x73, 0x65,
	0x72, 0x49, 0x64, 0x73, 0x12, 0x23, 0x0a, 0x04, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0f, 0x2e, 0x70, 0x62, 0x2e, 0x4d, 0x61, 0x69, 0x6c, 0x42, 0x6f, 0x78, 0x44,
	0x61, 0x74, 0x61, 0x52, 0x04, 0x6d, 0x61, 0x69, 0x6c, 0x22, 0x4d, 0x0a, 0x1c, 0x43, 0x68, 0x65,
	0x63, 0x6b, 0x53, 0x65, 0x6e, 0x74, 0x4d, 0x61, 0x69, 0x6c, 0x42, 0x6f, 0x78, 0x73, 0x54, 0x6f,
	0x53, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x52, 0x65, 0x71, 0x12, 0x2d, 0x0a, 0x07, 0x73, 0x6f, 0x75,
	0x72, 0x63, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x70, 0x62, 0x2e,
	0x53, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x4d, 0x61, 0x69, 0x6c, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x52,
	0x07, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x73, 0x22, 0x67, 0x0a, 0x0f, 0x53, 0x6f, 0x75, 0x72,
	0x63, 0x65, 0x4d, 0x61, 0x69, 0x6c, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x12, 0x1a, 0x0a, 0x08, 0x73,
	0x6f, 0x75, 0x72, 0x63, 0x65, 0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x73,
	0x6f, 0x75, 0x72, 0x63, 0x65, 0x49, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x73, 0x6f, 0x75, 0x72, 0x63,
	0x65, 0x54, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x73, 0x6f, 0x75,
	0x72, 0x63, 0x65, 0x54, 0x79, 0x70, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6f, 0x77, 0x6e, 0x65, 0x72,
	0x49, 0x44, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x77, 0x6e, 0x65, 0x72, 0x49,
	0x44, 0x22, 0xa5, 0x01, 0x0a, 0x1d, 0x43, 0x68, 0x65, 0x63, 0x6b, 0x53, 0x65, 0x6e, 0x74, 0x4d,
	0x61, 0x69, 0x6c, 0x42, 0x6f, 0x78, 0x73, 0x54, 0x6f, 0x53, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x52,
	0x65, 0x73, 0x70, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61,
	0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67,
	0x65, 0x12, 0x27, 0x0a, 0x04, 0x73, 0x65, 0x6e, 0x74, 0x18, 0x03, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x13, 0x2e, 0x70, 0x62, 0x2e, 0x53, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x4d, 0x61, 0x69, 0x6c, 0x45,
	0x76, 0x65, 0x6e, 0x74, 0x52, 0x04, 0x73, 0x65, 0x6e, 0x74, 0x12, 0x2d, 0x0a, 0x07, 0x6e, 0x6f,
	0x74, 0x53, 0x65, 0x6e, 0x74, 0x18, 0x04, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x13, 0x2e, 0x70, 0x62,
	0x2e, 0x53, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x4d, 0x61, 0x69, 0x6c, 0x45, 0x76, 0x65, 0x6e, 0x74,
	0x52, 0x07, 0x6e, 0x6f, 0x74, 0x53, 0x65, 0x6e, 0x74, 0x22, 0xc1, 0x01, 0x0a, 0x0d, 0x4d, 0x61,
	0x69, 0x6c, 0x42, 0x6f, 0x78, 0x41, 0x74, 0x74, 0x61, 0x63, 0x68, 0x12, 0x10, 0x0a, 0x03, 0x6b,
	0x65, 0x79, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6b, 0x65, 0x79, 0x12, 0x12, 0x0a,
	0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x79, 0x70,
	0x65, 0x12, 0x18, 0x0a, 0x07, 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12,
	0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x69, 0x73, 0x43, 0x6c, 0x61, 0x69, 0x6d,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x08, 0x52, 0x07, 0x69, 0x73, 0x43, 0x6c, 0x61, 0x69, 0x6d, 0x12,
	0x10, 0x0a, 0x03, 0x75, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x69,
	0x64, 0x12, 0x1a, 0x0a, 0x08, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x49, 0x64, 0x18, 0x08, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x08, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x49, 0x64, 0x22, 0xec, 0x04,
	0x0a, 0x0b, 0x4d, 0x61, 0x69, 0x6c, 0x42, 0x6f, 0x78, 0x44, 0x61, 0x74, 0x61, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a,
	0x04, 0x66, 0x72, 0x6f, 0x6d, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x66, 0x72, 0x6f,
	0x6d, 0x12, 0x0e, 0x0a, 0x02, 0x74, 0x6f, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x74,
	0x6f, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x62, 0x6f, 0x64, 0x79, 0x12, 0x20, 0x0a, 0x0b, 0x63,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x0b, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x20, 0x0a,
	0x0b, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x0b, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x12,
	0x33, 0x0a, 0x0b, 0x61, 0x74, 0x74, 0x61, 0x63, 0x68, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x18, 0x08,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x11, 0x2e, 0x70, 0x62, 0x2e, 0x4d, 0x61, 0x69, 0x6c, 0x42, 0x6f,
	0x78, 0x41, 0x74, 0x74, 0x61, 0x63, 0x68, 0x52, 0x0b, 0x61, 0x74, 0x74, 0x61, 0x63, 0x68, 0x6d,
	0x65, 0x6e, 0x74, 0x73, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x09,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1a, 0x0a, 0x08,
	0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x49, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x74, 0x65, 0x6d, 0x70,
	0x6c, 0x61, 0x74, 0x65, 0x55, 0x69, 0x64, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x74,
	0x65, 0x6d, 0x70, 0x6c, 0x61, 0x74, 0x65, 0x55, 0x69, 0x64, 0x12, 0x31, 0x0a, 0x0b, 0x66, 0x72,
	0x6f, 0x6d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x0f, 0x2e, 0x70, 0x62, 0x2e, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66, 0x6f,
	0x52, 0x0b, 0x66, 0x72, 0x6f, 0x6d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x2d, 0x0a,
	0x09, 0x74, 0x6f, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x0f, 0x2e, 0x70, 0x62, 0x2e, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66,
	0x6f, 0x52, 0x09, 0x74, 0x6f, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x12, 0x1a, 0x0a, 0x08,
	0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x49, 0x64, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08,
	0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x49, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x73, 0x6f, 0x75, 0x72,
	0x63, 0x65, 0x54, 0x79, 0x70, 0x65, 0x18, 0x0f, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x73, 0x6f,
	0x75, 0x72, 0x63, 0x65, 0x54, 0x79, 0x70, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x66, 0x72, 0x6f, 0x6d,
	0x4f, 0x77, 0x6e, 0x65, 0x72, 0x49, 0x64, 0x18, 0x10, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x66,
	0x72, 0x6f, 0x6d, 0x4f, 0x77, 0x6e, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1c, 0x0a, 0x09, 0x74, 0x6f,
	0x4f, 0x77, 0x6e, 0x65, 0x72, 0x49, 0x64, 0x18, 0x11, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x74,
	0x6f, 0x4f, 0x77, 0x6e, 0x65, 0x72, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x61, 0x64,
	0x41, 0x74, 0x18, 0x12, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x72, 0x65, 0x61, 0x64, 0x41, 0x74,
	0x12, 0x1c, 0x0a, 0x09, 0x72, 0x65, 0x61, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x18, 0x13, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x09, 0x72, 0x65, 0x61, 0x64, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x12, 0x1c,
	0x0a, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x42, 0x79, 0x18, 0x14, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x42, 0x79, 0x22, 0xfb, 0x01, 0x0a,
	0x0b, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x10, 0x0a, 0x03, 0x75, 0x72, 0x6c, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x75, 0x72, 0x6c, 0x12, 0x16, 0x0a, 0x06, 0x67, 0x65,
	0x6e, 0x64, 0x65, 0x72, 0x18, 0x05, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x67, 0x65, 0x6e, 0x64,
	0x65, 0x72, 0x12, 0x16, 0x0a, 0x06, 0x61, 0x63, 0x74, 0x69, 0x76, 0x65, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x08, 0x52, 0x06, 0x61, 0x63, 0x74, 0x69, 0x76, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6f, 0x77,
	0x6e, 0x65, 0x72, 0x49, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x77, 0x6e,
	0x65, 0x72, 0x49, 0x64, 0x12, 0x26, 0x0a, 0x0e, 0x6f, 0x77, 0x6e, 0x65, 0x72, 0x49, 0x64, 0x49,
	0x6e, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x6f, 0x77,
	0x6e, 0x65, 0x72, 0x49, 0x64, 0x49, 0x6e, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x12, 0x10, 0x0a, 0x03,
	0x59, 0x6f, 0x42, 0x18, 0x09, 0x20, 0x01, 0x28, 0x05, 0x52, 0x03, 0x59, 0x6f, 0x42, 0x12, 0x1a,
	0x0a, 0x08, 0x53, 0x68, 0x61, 0x72, 0x65, 0x41, 0x67, 0x65, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x08,
	0x52, 0x08, 0x53, 0x68, 0x61, 0x72, 0x65, 0x41, 0x67, 0x65, 0x42, 0x3f, 0x5a, 0x3d, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x74, 0x6f, 0x70, 0x65, 0x62, 0x6f, 0x78,
	0x5f, 0x70, 0x61, 0x63, 0x6b, 0x61, 0x67, 0x65, 0x73, 0x2f, 0x6d, 0x69, 0x6d, 0x69, 0x69, 0x6e,
	0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x63, 0x6c, 0x69, 0x65, 0x6e, 0x74, 0x67, 0x72, 0x70, 0x63,
	0x2f, 0x70, 0x62, 0x2f, 0x6d, 0x61, 0x69, 0x6c, 0x62, 0x6f, 0x78, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_mailbox_proto_rawDescOnce sync.Once
	file_mailbox_proto_rawDescData = file_mailbox_proto_rawDesc
)

func file_mailbox_proto_rawDescGZIP() []byte {
	file_mailbox_proto_rawDescOnce.Do(func() {
		file_mailbox_proto_rawDescData = protoimpl.X.CompressGZIP(file_mailbox_proto_rawDescData)
	})
	return file_mailbox_proto_rawDescData
}

var file_mailbox_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_mailbox_proto_goTypes = []interface{}{
	(*CreateMailsResponse)(nil),           // 0: pb.CreateMailsResponse
	(*CreateMailsRequest)(nil),            // 1: pb.CreateMailsRequest
	(*CheckSentMailBoxsToSourceReq)(nil),  // 2: pb.CheckSentMailBoxsToSourceReq
	(*SourceMailEvent)(nil),               // 3: pb.SourceMailEvent
	(*CheckSentMailBoxsToSourceResp)(nil), // 4: pb.CheckSentMailBoxsToSourceResp
	(*MailBoxAttach)(nil),                 // 5: pb.MailBoxAttach
	(*MailBoxData)(nil),                   // 6: pb.MailBoxData
	(*ProfileInfo)(nil),                   // 7: pb.ProfileInfo
}
var file_mailbox_proto_depIdxs = []int32{
	6, // 0: pb.CreateMailsRequest.mail:type_name -> pb.MailBoxData
	3, // 1: pb.CheckSentMailBoxsToSourceReq.sources:type_name -> pb.SourceMailEvent
	3, // 2: pb.CheckSentMailBoxsToSourceResp.sent:type_name -> pb.SourceMailEvent
	3, // 3: pb.CheckSentMailBoxsToSourceResp.notSent:type_name -> pb.SourceMailEvent
	5, // 4: pb.MailBoxData.attachments:type_name -> pb.MailBoxAttach
	7, // 5: pb.MailBoxData.fromProfile:type_name -> pb.ProfileInfo
	7, // 6: pb.MailBoxData.toProfile:type_name -> pb.ProfileInfo
	7, // [7:7] is the sub-list for method output_type
	7, // [7:7] is the sub-list for method input_type
	7, // [7:7] is the sub-list for extension type_name
	7, // [7:7] is the sub-list for extension extendee
	0, // [0:7] is the sub-list for field type_name
}

func init() { file_mailbox_proto_init() }
func file_mailbox_proto_init() {
	if File_mailbox_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_mailbox_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateMailsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateMailsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CheckSentMailBoxsToSourceReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SourceMailEvent); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CheckSentMailBoxsToSourceResp); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MailBoxAttach); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*MailBoxData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_mailbox_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProfileInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_mailbox_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_mailbox_proto_goTypes,
		DependencyIndexes: file_mailbox_proto_depIdxs,
		MessageInfos:      file_mailbox_proto_msgTypes,
	}.Build()
	File_mailbox_proto = out.File
	file_mailbox_proto_rawDesc = nil
	file_mailbox_proto_goTypes = nil
	file_mailbox_proto_depIdxs = nil
}
