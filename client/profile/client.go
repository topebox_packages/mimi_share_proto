package profile

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/profile/model"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/utils"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/profile"
	pbprofile "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/profile"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type profileClientInternalService struct {
	client pbprofile.ProfileServiceClient
}
type ProfileInternalServices struct {
	IProfileInternalService
	client       pb.ProfileServiceClient
	configClient InitProfileClientParams
}

type InitProfileClientParams struct {
	Url          string
	ExpireSecond int64
}

func NewProfileClientInternalService(url string, expireRequest int64) *profileClientInternalService {
	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) * time.Second // Set your desired timeout duration
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	var (
		cred = insecure.NewCredentials()
	)

	conn, err := grpc.DialContext(ctx, url, grpc.WithTransportCredentials(cred))
	if err != nil {
		log.Printf("dial grpc to profile service error: %v", err)
		return nil
	}
	log.Println("dial grpc to profile service successfully")
	return &profileClientInternalService{
		client: pbprofile.NewProfileServiceClient(conn),
	}
}

func GetProfileInternalService(url string, expireRequest int64) *ProfileInternalServices {
	var profileSvc *ProfileInternalServices

	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) // Set your desired timeout duration

	profileSvc = GetProfileInternalServiceWithParams(utils.InitClientParams{
		Url:          url,
		ExpireSecond: int64(timeout),
	})
	log.Println("dial to mimiland profile internal service successfully")

	return profileSvc
}

func GetProfileInternalServiceWithParams(params utils.InitClientParams, opts ...grpc.DialOption) *ProfileInternalServices {
	var profileSvc *ProfileInternalServices

	if params.ExpireSecond == 0 {
		params.ExpireSecond = 120
	}
	timeout := time.Duration(params.ExpireSecond) * time.Second // Set your desired timeout duration

	// Create a context with the specified timeout
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	cred := insecure.NewCredentials()
	opts = append(opts, grpc.WithTransportCredentials(cred))
	retriableErrors := utils.DefaultRetriableErrors
	if len(params.RetriableErrors) > 0 {
		retriableErrors = append(retriableErrors, params.RetriableErrors...)
	}
	if params.RetryTime != 0 {
		unaryInterceptor := grpc_retry.UnaryClientInterceptor(grpc_retry.WithCodes(retriableErrors...), grpc_retry.WithMax(params.RetryTime), grpc_retry.WithPerRetryTimeout(params.PerRetryTimeout))
		opts = append(opts, grpc.WithUnaryInterceptor(unaryInterceptor))
	}
	conn, err := grpc.DialContext(ctx, params.Url, opts...)
	if err != nil {
		log.Println("cannot connect grpc profileSvc service")
		return nil
	}
	profileSvc = &ProfileInternalServices{
		client: pb.NewProfileServiceClient(conn),
		configClient: InitProfileClientParams{
			Url:          params.Url,
			ExpireSecond: int64(timeout),
		},
	}
	log.Println("dial to mimiland profile internal service successfully")

	return profileSvc
}

func (c *profileClientInternalService) Ping(ctx context.Context) (message string, err error) {
	resp, err := c.client.Ping(ctx, &pbprofile.PingProfileReq{})
	if err != nil {
		return
	}
	if resp.Code != 200 {
		err = errors.New(resp.Message)
		return
	}
	message = resp.Message
	return
}

func (c *profileClientInternalService) Home(ctx context.Context) (code int32, message string, err error) {
	resp, err := c.client.Home(ctx, &pbprofile.IndexProfileReq{})
	if err != nil {
		return
	}
	code = 200
	message = fmt.Sprintf("%s - %s", resp.Name, resp.Version)
	return
}

func (c *profileClientInternalService) GetProfileIncludes(ctx context.Context, req model.GetProfile) (resp *pbprofile.GetProfileReply, err error) {
	resp, err = c.client.GetMyProfile(ctx, &pbprofile.GetProfileReq{
		Includes: req.Includes,
		Id:       req.Id,
		OwnerId:  req.OwnerId,
	})
	if err != nil {
		return
	}
	if resp != nil {
		if resp.Code != 200 {
			err = errors.New(resp.Message)
			return
		}
		return
	}
	return
}

func (c *profileClientInternalService) AddOrUpdateProfile(ctx context.Context, param model.UpdateProfile) (reply *pbprofile.UpdateProfileReply, err error) {
	if param.Includes == "" {
		param.Includes = "*"
	}
	reply, err = c.client.UpdateProfile(ctx, &pbprofile.UpdateProfileReq{
		Includes: param.Includes,
		Name:     param.Name,
		Gender:   param.Gender,
		Url:      param.Url,
		YoB:      param.YoB,
		ShareAge: param.ShareAge,
		Status:   param.Status,
		Active:   fmt.Sprint(param.Active),
	})

	if err != nil {
		return
	}
	if reply != nil {
		if reply.Code != 200 {
			err = errors.New(reply.Message)
			return
		}
		return
	}
	return
}

func (c *profileClientInternalService) UpdateAvatar(ctx context.Context, avaItems model.ListUpdateAvatars) (reply *pbprofile.UpdateAvatarReply, err error) {
	if len(avaItems) <= 0 {
		return
	}
	var req = make([]*pbprofile.ReqAvatarItem, len(avaItems))
	for _, item := range avaItems {
		req = append(req, &pbprofile.ReqAvatarItem{
			Id:       item.Id,
			ItemId:   item.ItemId,
			Type:     item.Type,
			Order:    item.Order,
			DesignId: item.DesignId,
		})
	}

	reply, err = c.client.UpdateAvatar(ctx, &pbprofile.UpdateAvatarReq{Items: req})

	if err != nil {
		return
	}
	if reply != nil {
		if reply.Code != 200 {
			err = errors.New(reply.Message)
			return
		}
		return
	}
	return
}

func (c *profileClientInternalService) GetProfileByOwner(ctx context.Context, ownerId string) (reply *pbprofile.GetProfileReply, err error) {

	reply, err = c.client.GetProfileByOwner(ctx, &pbprofile.GetProfileReq{OwnerId: ownerId})

	if err != nil {
		return
	}
	if reply != nil {
		if reply.Code != 200 {
			err = errors.New(reply.Message)
			return
		}
		return
	}
	return
}

func (c *profileClientInternalService) GetProfileByIdInWorld(ctx context.Context, idInWorld string) (reply *pbprofile.GetProfileReply, err error) {

	reply, err = c.client.GetProfileByIdInWorld(ctx, &pbprofile.GetProfileReq{Id: idInWorld})

	if err != nil {
		return
	}
	if reply != nil {
		if reply.Code != 200 {
			err = errors.New(reply.Message)
			return
		}
		return
	}
	return
}

func (p *ProfileInternalServices) UpdateProfile(ctx context.Context, req *pb.UpdateProfileReq) (resp *pb.UpdateProfileReply, err error) {
	resp, err = p.client.UpdateProfile(ctx, req)
	return
}
func (c *profileClientInternalService) GetAvatar(ctx context.Context, avatarId, ownerId string) (reply *pbprofile.GetAvatarReply, err error) {
	reply, err = c.client.GetAvatar(ctx, &pbprofile.GetAvatarReq{
		Id:      avatarId,
		OwnerId: ownerId,
	})
	if err != nil {
		return
	}
	if reply != nil {
		if reply.Code != 200 {
			err = errors.New(reply.Message)
			return
		}
		return
	}
	return
}

func (p *ProfileInternalServices) GetProfileByOwner(ctx context.Context, req *pb.GetProfileReq) (resp *pb.GetProfileReply, err error) {
	resp, err = p.client.GetProfileByOwner(ctx, req)
	return
}
func (p *ProfileInternalServices) GetProfileByIdInWorld(ctx context.Context, req *pb.GetProfileReq) (resp *pb.GetProfileReply, err error) {
	resp, err = p.client.GetProfileByIdInWorld(ctx, req)
	return
}
