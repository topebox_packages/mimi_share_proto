upload:
	redoc-cli build -o ./documents/swagger/index.html ./documents/swagger/mimiland-cdn.swagger.json
	gcloud storage cp ./documents/swagger/index.html gs://topedevcdn/docs/mimiland-cdn/index.html
	gsutil iam ch allUsers:legacyObjectReader gs://topedevcdn/docs/mimiland-cdn/index.html
	gsutil iam ch serviceAccount:service-150586831608@cloud-cdn-fill.iam.gserviceaccount.com:objectViewer gs://mimilanddev_staticdata

pro-cdn:
	protoc --go_out=pd/cdn --go-grpc_out=pd/cdn \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/cdn proto/cdn/*.proto \

upload:
	redoc-cli build -o ./documents/swagger/index.html ./documents/swagger/mimiland-cdn.swagger.json
	gcloud storage cp ./documents/swagger/index.html gs://topedevcdn/docs/mimiland-cdn/index.html
	gsutil iam ch allUsers:legacyObjectReader gs://topedevcdn/docs/mimiland-cdn/index.html
	gsutil iam ch serviceAccount:service-150586831608@cloud-cdn-fill.iam.gserviceaccount.com:objectViewer gs://mimilanddev_staticdata

pro-leaderboard:
	protoc --go_out=pd/leaderboard --go-grpc_out=pd/leaderboard \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/leaderboard proto/leaderboard/*.proto

pro-auth:
	protoc --go_out=pd/auth --go-grpc_out=pd/auth \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/auth proto/auth/*.proto

pro-friend:
	protoc --go_out=pd/friends --go-grpc_out=pd/friends \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/friends proto/friends/*.proto

pro-inventory:
	protoc --go_out=pd/inventory --go-grpc_out=pd/inventory \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/inventory proto/inventory/*.proto

pro-profile:
	protoc --go_out=pd/profile --go-grpc_out=pd/profile \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/profile proto/profile/*.proto
pro-mailbox:
	protoc --go_out=pd/mailbox --go-grpc_out=pd/mailbox \
	--go_opt=paths=source_relative \
	--go-grpc_opt=paths=source_relative \
	--proto_path=proto/mailbox proto/mailbox/*.proto

test:
	go test -v $(dir)