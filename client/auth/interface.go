package profile

import (
	"context"

	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/auth"
)

type IAuthInternalService interface {
	Ping(ctx context.Context) (resp *pb.AuthPingReply, err error)
	RevokeAppleId(ctx context.Context, req *pb.RevokeAppleTokenReq) (resp *pb.RevokeAppleTokenReply, err error)
	UnLinkAccount(ctx context.Context, req *pb.AccountLinkReq) (resp *pb.AccountLinkReply, err error)
	GetSession(ctx context.Context, req *pb.GetSessionReq) (resp *pb.GetSessionReply, err error)
	GetSessionV2(ctx context.Context, req *pb.GetSessionReq) (resp *pb.GetSessionV2Reply, err error)
	GetUserInfo(ctx context.Context, req *pb.GetUserInfoReq) (resp *pb.GetUserInfoReply, err error)
	GetUserInfoV2(ctx context.Context, req *pb.GetUserInfoReq) (resp *pb.GetUserInfoV2Reply, err error)
}
