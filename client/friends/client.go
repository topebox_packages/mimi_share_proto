package friends

import (
	"context"
	"errors"
	"log"
	"time"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/friends/model"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/friends"
	pdfriends "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/friends"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type FriendsInternalServices struct {
	IFriendsInternalServices
	client       pb.MimilandServiceClient
	configClient InitFriendsClientParams
}

type InitFriendsClientParams struct {
	Url          string
	ExpireSecond int64
}

// NewFriendsServiceClient create new friends service client with grpc connection
// url: url of grpc server
// expireRequest: expire request time in second
// func NewFriendsServiceClient(url string, expireRequest int64) *FriendsInternalServices {
// 	timeout := time.Duration(expireRequest) * time.Second // Set your desired timeout duration

// 	// Create a context with the specified timeout
// 	ctx, cancel := context.WithTimeout(context.Background(), timeout)
// 	defer cancel()

// 	conn, err := grpc.DialContext(ctx, url, grpc.WithTransportCredentials(insecure.NewCredentials()))
// 	if err != nil {
// 		log.Println("cannot connect grpc friends service", err)
// 		return nil
// 	}
// 	return &FriendsInternalServices{
// 		client: pb.NewMimilandServiceClient(conn),
// 		configClient: InitFriendsClientParams{
// 			Url:          url,
// 			ExpireSecond: expireRequest,
// 		},
// 	}
// }

// url: url of grpc server
// expireRequest: expire request time in second

func GetFriendsInternalService(url string, expireRequest int64) *FriendsInternalServices {
	var friendSvc *FriendsInternalServices

	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) * time.Second // Set your desired timeout duration

	// Create a context with the specified timeout
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	cred := insecure.NewCredentials()
	conn, err := grpc.DialContext(ctx, url, grpc.WithTransportCredentials(cred))
	if err != nil {
		log.Println("cannot connect grpc profileSvc service")
		return nil
	}
	friendSvc = &FriendsInternalServices{
		client: pb.NewMimilandServiceClient(conn),
		configClient: InitFriendsClientParams{
			Url:          url,
			ExpireSecond: expireRequest,
		},
	}
	log.Println("dial to mimiland friends internal service successfully")

	return friendSvc
}

func (fs *FriendsInternalServices) Ping(ctx context.Context) (message string, err error) {
	// var cancelFunc context.CancelFunc
	// if fs.configClient.ExpireSecond != 0 {
	// 	ctx, cancelFunc = context.WithTimeout(ctx, time.Duration(fs.configClient.ExpireSecond)*time.Second)
	// 	defer cancelFunc()
	// }
	resp, err := fs.client.Ping(ctx, &pb.PingReq{})
	if err != nil {
		return "", err
	}
	if resp != nil {
		if resp.Code != 200 {
			return "", errors.New(resp.Message)
		}
		return resp.Message, nil
	}
	return
}

func (fs *FriendsInternalServices) BecomeFriend(ctx context.Context, in *model.BecomeFriendRequest) (resp *pdfriends.BecomeFriendReply, err error) {
	// var cancelFunc context.CancelFunc
	// if fs.configClient.ExpireSecond != 0 {
	// 	ctx, cancelFunc = context.WithTimeout(ctx, time.Duration(fs.configClient.ExpireSecond)*time.Second)
	// 	defer cancelFunc()
	// }
	resp, err = fs.client.BecomeFriend(ctx, &pb.BecomeFriendReq{
		OwnerId:  in.OwnerId,
		FriendId: in.FriendId,
	})
	if err != nil {
		return
	}
	return
}

func (fs *FriendsInternalServices) RemoveFriend(ctx context.Context, in *model.RemoveFriendParam) (resp *pdfriends.RemoveFriendReply, err error) {
	// var cancelFunc context.CancelFunc
	// if fs.configClient.ExpireSecond != 0 {
	// 	ctx, cancelFunc = context.WithTimeout(ctx, time.Duration(fs.configClient.ExpireSecond)*time.Second)
	// 	defer cancelFunc()
	// }

	resp, err = fs.client.RemoveFriend(ctx, &pdfriends.RemoveFriendReq{
		FriendId: in.FriendId,
	})
	if err != nil {
		return
	}
	return
}

func (fs *FriendsInternalServices) Is(ctx context.Context, in *pdfriends.IsCheckFriendReq) (resp *pdfriends.IsCheckFriendReply, err error) {

	resp, err = fs.client.Is(ctx, in)
	if err != nil {
		return
	}
	return
}
