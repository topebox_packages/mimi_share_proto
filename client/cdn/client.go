package cdn

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"time"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/cdn/model"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/utils"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/cdn"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
)

var _ IMimilandCDNItemDesignServiceClient
var _ IMimilandCDNServiceClient
var (
	retriableErrors = []codes.Code{codes.Internal, codes.DeadlineExceeded}
)

type CdnServiceClient struct {
	IMimilandCDNItemDesignServiceClient
	Client           cdn.MimilandCDNServiceClient
	ItemDesignClient cdn.MimilandCDNItemDesignServiceClient
	ConfigClient     utils.InitClientParams
}

var (
	cdnSvc *CdnServiceClient
)

func GetCDnService(params utils.InitClientParams, opts ...grpc.DialOption) *CdnServiceClient {
	if params.ExpireSecond == 0 {
		params.ExpireSecond = 120
	}
	timeout := time.Duration(params.ExpireSecond) * time.Second // Set your desired timeout duration

	// Create a context with the specified timeout
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	cred := insecure.NewCredentials()
	opts = append(opts, grpc.WithTransportCredentials(cred))
	if len(params.RetriableErrors) > 0 {
		retriableErrors = append(retriableErrors, params.RetriableErrors...)
	}
	if params.RetryTime != 0 {
		unaryInterceptor := grpc_retry.UnaryClientInterceptor(grpc_retry.WithCodes(retriableErrors...), grpc_retry.WithMax(params.RetryTime), grpc_retry.WithPerRetryTimeout(params.PerRetryTimeout))
		opts = append(opts, grpc.WithUnaryInterceptor(unaryInterceptor))
	}
	conn, err := grpc.DialContext(ctx, params.Url, opts...)
	if err != nil {
		log.Println("cannot connect grpc cdnSvc service")
		return nil
	}
	cdnSvc = &CdnServiceClient{
		Client:           cdn.NewMimilandCDNServiceClient(conn),
		ItemDesignClient: cdn.NewMimilandCDNItemDesignServiceClient(conn),
		ConfigClient:     params,
	}
	log.Println("connect mimiland cdnSvc service successfully")
	return cdnSvc
}

func (c *CdnServiceClient) Ping(ctx context.Context, in *cdn.CDNPingReq, opts ...grpc.CallOption) (resp *cdn.CDNPingReply, err error) {
	resp, err = c.Client.Ping(ctx, in)
	return resp, err
}

func (c *CdnServiceClient) Index(ctx context.Context, in *cdn.CDNIndexReq, opts ...grpc.CallOption) (resp *cdn.CDNIndexReply, err error) {
	return c.Client.Index(ctx, in, opts...)
}
func (c *CdnServiceClient) GetTimeServer(ctx context.Context, in *cdn.GetCDNTimeServerReq, opts ...grpc.CallOption) (resp *cdn.GetCDNTimeServerReply, err error) {
	return c.Client.GetTimeServer(ctx, in, opts...)
}
func (c *CdnServiceClient) UploadImage(ctx context.Context, req *model.UploadImageRequest, opts ...grpc.CallOption) (resp *model.UploadImageResponse, err error) {
	byteData, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	pb := &cdn.UploadImageRequest{}
	err = json.Unmarshal(byteData, &pb)
	if err != nil {
		return nil, err
	}
	stream, err := c.Client.UploadImage(ctx, opts...)
	if err != nil {
		return nil, err
	}
	if err = stream.Send(pb); err != nil {
		return nil, err
	}
	buffer := make([]byte, 1024)
	for {
		n, err := req.FileReader.Read(buffer)
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal("cannot read chunk to buffer: ", err)
		}

		req := &cdn.UploadImageRequest{
			Data: &cdn.UploadImageRequest_ChunkData{
				ChunkData: buffer[:n],
			},
		}

		err = stream.Send(req)
		if err != nil {
			log.Fatal("cannot send chunk to server: ", err, stream.RecvMsg(nil))
		}
	}

	respPb, err := stream.CloseAndRecv()
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}

	respByteData, err := json.Marshal(respPb)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respByteData, &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *CdnServiceClient) GetLinkImage(ctx context.Context, in *cdn.GetLinkImageRequest, opts ...grpc.CallOption) (resp *cdn.GetLinkImageResponse, err error) {
	return c.Client.GetLinkImage(ctx, in, opts...)
}

func (c *CdnServiceClient) GetItemDesign(ctx context.Context, req *model.GetItemDesignRequest, opts ...grpc.CallOption) (resp *model.GetItemDesignResponse, err error) {
	byteData, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	pb := &cdn.GetItemDesignRequest{}
	err = json.Unmarshal(byteData, &pb)
	if err != nil {
		return nil, err
	}
	respPb, err := c.ItemDesignClient.GetItemDesign(ctx, pb, opts...)
	if err != nil {
		return nil, err
	}
	respByteData, err := json.Marshal(respPb)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respByteData, &resp)
	if err != nil {
		return nil, err
	}

	return
}

func (c *CdnServiceClient) UploadItemDesign(ctx context.Context, req *model.UploadItemDesignRequest, opts ...grpc.CallOption) (resp *model.UploadItemDesignResponse, err error) {
	byteData, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	pb := &cdn.UploadItemDesignRequest{}
	err = json.Unmarshal(byteData, &pb)
	if err != nil {
		return nil, err
	}
	stream, err := c.ItemDesignClient.UploadItemDesign(ctx, opts...)
	if err != nil {
		return nil, err
	}

	if err = stream.Send(pb); err != nil {
		return nil, err
	}
	buffer := make([]byte, 1024)
	for {
		n, err := req.FileReader.Read(buffer)
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal("cannot read chunk to buffer: ", err)
		}

		req := &cdn.UploadItemDesignRequest{
			Data: &cdn.UploadItemDesignRequest_ChunkData{
				ChunkData: buffer[:n],
			},
		}

		err = stream.Send(req)
		if err != nil {
			log.Fatal("cannot send chunk to server: ", err, stream.RecvMsg(nil))
		}
	}

	respPb, err := stream.CloseAndRecv()
	if err != nil {
		return nil, err
	}

	respByteData, err := json.Marshal(respPb)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respByteData, &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *CdnServiceClient) UpdateItemDesign(ctx context.Context, req *model.UpdateItemDesignRequest, opts ...grpc.CallOption) (resp *model.UpdateItemDesignResponse, err error) {
	byteData, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	pb := &cdn.UpdateItemDesignRequest{}
	err = json.Unmarshal(byteData, &pb)
	if err != nil {
		return nil, err
	}
	stream, err := c.ItemDesignClient.UpdateItemDesign(ctx, opts...)
	if err != nil {
		return nil, err
	}

	if err = stream.Send(pb); err != nil {
		return nil, err
	}
	buffer := make([]byte, 1024)
	for {
		n, err := req.FileReader.Read(buffer)
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal("cannot read chunk to buffer: ", err)
		}

		req := &cdn.UpdateItemDesignRequest{
			Data: &cdn.UpdateItemDesignRequest_ChunkData{
				ChunkData: buffer[:n],
			},
		}

		err = stream.Send(req)
		if err != nil {
			log.Fatal("cannot send chunk to server: ", err, stream.RecvMsg(nil))
		}
	}

	respPb, err := stream.CloseAndRecv()
	if err != nil {
		return nil, err
	}
	respByteData, err := json.Marshal(respPb)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respByteData, &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *CdnServiceClient) DeleteItemDesign(ctx context.Context, req *model.DeleteItemDesignRequest, opts ...grpc.CallOption) (resp *model.DeleteItemDesignResponse, err error) {
	byteData, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	pb := &cdn.DeleteItemDesignRequest{}
	err = json.Unmarshal(byteData, &pb)
	if err != nil {
		return nil, err
	}
	respPb, err := c.ItemDesignClient.DeleteItemDesign(ctx, pb, opts...)
	if err != nil {
		return nil, err
	}
	respByteData, err := json.Marshal(respPb)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respByteData, &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (c *CdnServiceClient) GetListObject(ctx context.Context, req *model.GetListObjectRequest, opts ...grpc.CallOption) (resp *model.GetListObjectResponse, err error) {
	byteData, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	pb := &cdn.GetListObjectRequest{}
	err = json.Unmarshal(byteData, &pb)
	if err != nil {
		return nil, err
	}
	respPb, err := c.ItemDesignClient.GetListObject(ctx, pb, opts...)
	if err != nil {
		return nil, err
	}
	respByteData, err := json.Marshal(respPb)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respByteData, &resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
