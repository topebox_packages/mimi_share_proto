package friends

import (
	"context"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/friends/model"
	pdfriends "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/friends"
)

type IFriendsInternalServices interface {
	Ping(ctx context.Context) (message string, err error)
	BecomeFriend(ctx context.Context, in *model.BecomeFriendRequest) (resp *pdfriends.BecomeFriendReply, err error)
	RemoveFriend(ctx context.Context, in *model.RemoveFriendParam) (resp *pdfriends.RemoveFriendReply, err error)
	Is(ctx context.Context, in *pdfriends.IsCheckFriendReq) (resp *pdfriends.IsCheckFriendReply, err error)
}
