package utils

import (
	"time"

	"google.golang.org/grpc/codes"
)

var (
	DefaultRetriableErrors = []codes.Code{codes.Internal, codes.DeadlineExceeded}
)

type InitClientParams struct {
	Url             string
	ExpireSecond    int64
	RetryTime       uint
	PerRetryTimeout time.Duration
	RetriableErrors []codes.Code
}
