package leaderboard

import (
	"context"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/leaderboard/model"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/leaderboard"
)

type ILeaderBoardInternalService interface {
	Ping(ctx context.Context) (message string, err error)
	GetLeaderBoard(ctx context.Context, req model.GetLeaderBoardParamModel) (data []*pb.LB_Data, err error)
	GetLeaderBoardV2(ctx context.Context, req model.GetLeaderBoardParamModel) (data, aroundData []*pb.LB_Data, ownerData *pb.LB_Data, err error)
	GetLeaderBoardV3(ctx context.Context, req model.GetLeaderBoardParamModel) (data, aroundData []*pb.LB_Data, ownerData *pb.LB_Data, err error)
	ClearData(ctx context.Context, ownerId, clientId, name string) (success bool, err error)
	ResetData(ctx context.Context) (success bool, err error)
}
