package mailbox

import (
	"context"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/mailbox/model"
	"google.golang.org/grpc"
)

type IMailboxInternalService interface {
	Ping(ctx context.Context) (message string, err error)
	DevCreateMails(ctx context.Context, in *model.CreateMailsRequest, opts ...grpc.CallOption) (resp *model.CreateMailsResponse, err error)
	ApiKeyCreateMails(ctx context.Context, in *model.CreateMailsRequest, opts ...grpc.CallOption) (resp *model.CreateMailsResponse, err error)
	InternalCreateMails(ctx context.Context, in *model.CreateMailsRequest, opts ...grpc.CallOption) (resp *model.CreateMailsResponse, err error)
	DevCheckSentMailBoxsToSource(ctx context.Context, req *model.CheckSentMailBoxsToSourceReq, opts ...grpc.CallOption) (resp *model.CheckSentMailBoxsToSourceResp, err error)
	ApiKeyCheckSentMailBoxsToSource(ctx context.Context, req *model.CheckSentMailBoxsToSourceReq, opts ...grpc.CallOption) (resp *model.CheckSentMailBoxsToSourceResp, err error)
	InternalCheckSentMailBoxsToSource(ctx context.Context, req *model.CheckSentMailBoxsToSourceReq, opts ...grpc.CallOption) (resp *model.CheckSentMailBoxsToSourceResp, err error)
}
