package profile

import (
	"context"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/profile/model"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/profile"
	pbprofile "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/profile"
)

type IProfileInternalService interface {
	Home(ctx context.Context) (resp *pb.IndexProfileReply, err error)
	GetProfileIncludes(ctx context.Context, req model.GetProfile) (resp *pbprofile.GetProfileReply, err error)
	AddOrUpdateProfile(ctx context.Context, param model.UpdateProfile) (reply *pbprofile.UpdateProfileReply, err error)
	UpdateProfile(ctx context.Context, req *pb.UpdateProfileReq) (resp *pb.UpdateProfileReply, err error)
	//UpdateAvatar(ctx context.Context, req *pb.UpdateAvatarReq) (resp *pb.UpdateAvatarReply, err error)
	// GetProfileByOwner(ctx context.Context, req *pb.GetProfileReq) (resp *pb.GetProfileReply, err error)
	// GetProfileByIdInWorld(ctx context.Context, req *pb.GetProfileReq) (resp *pb.GetProfileReply, err error)

	UpdateAvatar(ctx context.Context, avaItems model.ListUpdateAvatars) (reply *pbprofile.UpdateAvatarReply, err error)
	GetProfileByOwner(ctx context.Context, ownerId string) (reply *pbprofile.GetProfileReply, err error)
	GetProfileByIdInWorld(ctx context.Context, idInWorld string) (reply *pbprofile.GetProfileReply, err error)
	GetAvatar(ctx context.Context, avatarId, ownerId string) (reply *pbprofile.GetAvatarReply, err error)
}
