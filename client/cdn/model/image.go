package model

import "bufio"

type UploadImageRequest struct {
	ObjectType  string        `json:"objectType,omitempty" bson:"objectType,omitempty"`
	IsDuplicate bool          `json:"isDuplicate,omitempty" bson:"isDuplicate,omitempty"`
	FileReader  *bufio.Reader `json:"fileReader,omitempty" bson:"fileReader,omitempty"`
}

type UploadImageResponse struct {
	Code     int32  `json:"code,omitempty" bson:"code,omitempty"`
	Message  string `json:"message,omitempty" bson:"message,omitempty"`
	Filename string `json:"filename,omitempty" bson:"filename,omitempty"`
	Size     uint32 `json:"size,omitempty" bson:"size,omitempty"`
	Id       string `json:"id,omitempty" bson:"id,omitempty"`
	Url      string `json:"url,omitempty" bson:"url,omitempty"`
}
