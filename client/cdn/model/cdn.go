package model

import "bufio"

type UploadItemDesignRequest struct {
	ObjectType  string        `json:"objectType,omitempty" bson:"objectType,omitempty"`
	ObjectId    string        `json:"objectId,omitempty" bson:"objectId,omitempty"`
	FeatureType string        `json:"featureType,omitempty" bson:"featureType,omitempty"`
	FileReader  *bufio.Reader `json:"fileReader,omitempty" bson:"fileReader,omitempty"`
}

type UploadItemDesignResponse struct {
	Code     int32  `json:"code,omitempty" bson:"code,omitempty"`
	Message  string `json:"message,omitempty" bson:"message,omitempty"`
	Id       string `json:"id,omitempty" bson:"id,omitempty"`
	Filename string `json:"filename,omitempty" bson:"filename,omitempty"`
	Url      string `json:"url,omitempty" bson:"url,omitempty"`
	Md5      string `json:"md5,omitempty" bson:"md5,omitempty"`
}

type GetItemDesignRequest struct {
	Id          string `json:"id,omitempty" bson:"id,omitempty"`
	FeatureType string `json:"featureType,omitempty" bson:"featureType,omitempty"`
}

type UpdateItemDesignRequest struct {
	ObjectType  string        `json:"objectType,omitempty" bson:"objectType,omitempty"`
	ObjectId    string        `json:"objectId,omitempty" bson:"objectId,omitempty"`
	FeatureType string        `json:"featureType,omitempty" bson:"featureType,omitempty"`
	FileReader  *bufio.Reader `json:"fileReader,omitempty" bson:"fileReader,omitempty"`
	Id          string        `json:"id,omitempty" bson:"id,omitempty"`
}

type UpdateItemDesignResponse struct {
	Code     int32  `json:"code,omitempty" bson:"code,omitempty"`
	Message  string `json:"message,omitempty" bson:"message,omitempty"`
	Id       string `json:"id,omitempty" bson:"id,omitempty"`
	Filename string `json:"filename,omitempty" bson:"filename,omitempty"`
	Url      string `json:"url,omitempty" bson:"url,omitempty"`
	Md5      string `json:"md5,omitempty" bson:"md5,omitempty"`
}

type DeleteItemDesignRequest struct {
	Id          string `json:"id,omitempty" bson:"id,omitempty"`
	FeatureType string `json:"featureType,omitempty" bson:"featureType,omitempty"`
}

type DeleteItemDesignResponse struct {
	Code    int32  `json:"code,omitempty" bson:"code,omitempty"`
	Message string `json:"message,omitempty" bson:"message,omitempty"`
}

type GetListObjectRequest struct {
	ObjectId    string   `json:"objectId,omitempty" bson:"objectId,omitempty"`
	ObjectType  string   `json:"objectType,omitempty" bson:"objectType,omitempty"`
	OwnerId     string   `json:"ownerId,omitempty" bson:"ownerId,omitempty"`
	FeatureType string   `json:"featureType,omitempty" bson:"featureType,omitempty"`
	Ids         []string `json:"ids,omitempty" bson:"ids,omitempty"`
}

type ObjectItemResponse struct {
	Id          string `json:"id,omitempty" bson:"id,omitempty"`
	ObjectType  string `json:"objectType,omitempty" bson:"objectType,omitempty"`
	ObjectId    string `json:"objectId,omitempty" bson:"objectId,omitempty"`
	Url         string `json:"url,omitempty" bson:"url,omitempty"`
	Md5         string `json:"md5,omitempty" bson:"md5,omitempty"`
	FeatureType string `json:"featureType,omitempty" bson:"featureType,omitempty"`
}

type GetListObjectResponse struct {
	Code    int32                 `json:"code,omitempty" bson:"code,omitempty"`
	Message string                `json:"message,omitempty" bson:"message,omitempty"`
	List    []*ObjectItemResponse `json:"list,omitempty" bson:"list,omitempty"`
}

type GetItemDesignResponse struct {
	Code        int32  `json:"code,omitempty" bson:"code,omitempty"`
	Message     string `json:"message,omitempty" bson:"message,omitempty"`
	Url         string `json:"url,omitempty" bson:"url,omitempty"`
	ObjectType  string `json:"objectType,omitempty" bson:"objectType,omitempty"`
	ObjectId    string `json:"objectId,omitempty" bson:"objectId,omitempty"`
	Id          string `json:"id,omitempty" bson:"id,omitempty"`
	Md5         string `json:"md5,omitempty" bson:"md5,omitempty"`
	FeatureType string `json:"featureType,omitempty" bson:"featureType,omitempty"`
}
