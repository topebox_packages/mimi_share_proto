package cdn

import (
	"bufio"
	"context"

	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/cdn/model"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/cdn"
	"google.golang.org/grpc"
)

type IMimilandCDNServiceClient interface {
	Ping(ctx context.Context, in *cdn.CDNPingReq, opts ...grpc.CallOption) (*cdn.CDNPingReply, error)
	Index(ctx context.Context, in *cdn.CDNIndexReq, opts ...grpc.CallOption) (resp *cdn.CDNIndexReply, err error)
	GetTimeServer(ctx context.Context, in *cdn.GetCDNTimeServerReq, opts ...grpc.CallOption) (resp *cdn.GetCDNTimeServerReply, err error)
	UploadImage(ctx context.Context, req *cdn.UploadImageRequest, reader *bufio.Reader, opts ...grpc.CallOption) (resp cdn.MimilandCDNService_UploadImageClient, err error)
	GetLinkImage(ctx context.Context, in *cdn.GetLinkImageRequest, opts ...grpc.CallOption) (resp *cdn.GetLinkImageResponse, err error)
}

type IMimilandCDNItemDesignServiceClient interface {
	UploadItemDesign(ctx context.Context, req *model.UploadItemDesignRequest, reader *bufio.Reader, opts ...grpc.CallOption) (*model.UploadItemDesignResponse, error)
	UpdateItemDesign(ctx context.Context, req *model.UpdateItemDesignRequest, reader *bufio.Reader, opts ...grpc.CallOption) (*model.UpdateItemDesignResponse, error)
	GetItemDesign(ctx context.Context, in *model.GetItemDesignRequest, opts ...grpc.CallOption) (*model.GetItemDesignResponse, error)
	DeleteItemDesign(ctx context.Context, in *model.DeleteItemDesignRequest, opts ...grpc.CallOption) (*model.DeleteItemDesignResponse, error)
	GetListObject(ctx context.Context, in *model.GetListObjectRequest, opts ...grpc.CallOption) (*model.GetListObjectResponse, error)
	UpdateFeatureConfig(ctx context.Context, in *cdn.UpdateFeatureConfigRequest, opts ...grpc.CallOption) (*cdn.UpdateFeatureConfigResponse, error)
	GetFeatureConfigByID(ctx context.Context, in *cdn.GetFeatureConfigByIDRequest, opts ...grpc.CallOption) (*cdn.GetFeatureConfigByIDResponse, error)
	GetListFeatureConfig(ctx context.Context, in *cdn.GetListFeatureConfigRequest, opts ...grpc.CallOption) (*cdn.GetListFeatureConfigResponse, error)
	CreateFeatureConfig(ctx context.Context, in *cdn.CreateFeatureConfigRequest, opts ...grpc.CallOption) (*cdn.CreateFeatureConfigResponse, error)
	DeleteFeatureConfigByID(ctx context.Context, in *cdn.DeleteFeatureConfigByIDRequest, opts ...grpc.CallOption) (*cdn.DeleteFeatureConfigByIDResponse, error)
}
