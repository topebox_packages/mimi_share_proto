package inventory

import (
	"context"
	"log"
	"time"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"gitlab.com/topebox_packages/mimiinternalclientgrpc/client/utils"
	pb "gitlab.com/topebox_packages/mimiinternalclientgrpc/pd/inventory"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type InventoryInternalServices struct {
	IInventoryInternalService
	client       pb.InventoryServiceClient
	configClient utils.InitClientParams
}

func GetInventoryInternalService(url string, expireRequest int64) *InventoryInternalServices {
	var inventorySvc *InventoryInternalServices
	if expireRequest == 0 {
		expireRequest = 120
	}
	timeout := time.Duration(expireRequest) // Set your desired timeout duration
	inventorySvc = GetInventoryInternalServiceWithParams(utils.InitClientParams{
		Url:          url,
		ExpireSecond: int64(timeout),
	})
	log.Println("dial to mimiland friends internal service successfully")

	return inventorySvc
}

func GetInventoryInternalServiceWithParams(params utils.InitClientParams, opts ...grpc.DialOption) *InventoryInternalServices {
	var inventorySvc *InventoryInternalServices
	timeout := time.Duration(params.ExpireSecond) * time.Second // Set your desired timeout duration

	// Create a context with the specified timeout
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	cred := insecure.NewCredentials()
	opts = append(opts, grpc.WithTransportCredentials(cred))
	retriableErrors := utils.DefaultRetriableErrors
	if len(params.RetriableErrors) > 0 {
		retriableErrors = append(retriableErrors, params.RetriableErrors...)
	}
	if params.RetryTime != 0 {
		unaryInterceptor := grpc_retry.UnaryClientInterceptor(grpc_retry.WithCodes(retriableErrors...), grpc_retry.WithMax(params.RetryTime), grpc_retry.WithPerRetryTimeout(params.PerRetryTimeout))
		opts = append(opts, grpc.WithUnaryInterceptor(unaryInterceptor))
	}
	conn, err := grpc.DialContext(ctx, params.Url, opts...)
	if err != nil {
		log.Println("cannot connect grpc inventorySvc service")
		return nil
	}
	inventorySvc = &InventoryInternalServices{
		client:       pb.NewInventoryServiceClient(conn),
		configClient: params,
	}
	log.Println("dial to mimiland friends internal service successfully")

	return inventorySvc
}

func (i *InventoryInternalServices) Home(ctx context.Context) (code int32, message string, err error) {
	_resp, err := i.client.Home(ctx, &pb.PingReq{})
	if err != nil {
		return
	}
	code = _resp.Code
	message = _resp.Message
	return
}

func (i *InventoryInternalServices) DevInitDesign(ctx context.Context, req *pb.DevInitDesignReq) (resp *pb.DevInitDesignResp, err error) {
	resp, err = i.client.DevInitDesign(ctx, req)
	return
}

func (i *InventoryInternalServices) DevPutDesign(ctx context.Context, req *pb.DevPutDesignReq) (resp *pb.DevPutDesignResp, err error) {
	resp, err = i.client.DevPutDesign(ctx, req)
	return
}

func (i *InventoryInternalServices) PutDesignAttributes(ctx context.Context, req *pb.PutDesignAttributesReq) (resp *pb.PutDesignAttributesResp, err error) {
	resp, err = i.client.PutDesignAttributes(ctx, req)
	return
}

func (i *InventoryInternalServices) DevBuildManifest(ctx context.Context, req *pb.DevBuildManifestReq) (resp *pb.DevBuildManifestResp, err error) {
	resp, err = i.client.DevBuildManifest(ctx, req)
	return
}

func (i *InventoryInternalServices) DevTransferOwner(ctx context.Context, req *pb.DevTransferOwnerReq) (resp *pb.DevTransferOwnerResp, err error) {
	resp, err = i.client.DevTransferOwner(ctx, req)
	return
}

func (i *InventoryInternalServices) AdminAddItems(ctx context.Context, req *pb.AdminAddItemsReq) (resp *pb.AdminAddItemsResp, err error) {
	resp, err = i.client.AdminAddItems(ctx, req)
	return
}

func (i *InventoryInternalServices) PutSharedItems(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error) {
	resp, err = i.client.PutSharedItems(ctx, req)
	return
}

func (i *InventoryInternalServices) ListDesign(ctx context.Context, req *pb.ListItemReqV2) (resp *pb.ListItemRespV2, err error) {
	resp, err = i.client.ListDesign(ctx, req)
	return
}

func (i *InventoryInternalServices) ListDesignV2(ctx context.Context, req *pb.ListItemReqV2) (resp *pb.ListItemRespV2, err error) {
	resp, err = i.client.ListDesignV2(ctx, req)
	return
}

func (i *InventoryInternalServices) GetItemByID(ctx context.Context, req *pb.GetItemByIDRequest) (resp *pb.GetItemByIDResponse, err error) {
	resp, err = i.client.GetItemByID(ctx, req)
	return
}

func (i *InventoryInternalServices) ListItemV3(ctx context.Context, req *pb.ListItemReqV3) (resp *pb.ListItemRespV3, err error) {
	resp, err = i.client.ListItemV3(ctx, req)
	return
}

func (i *InventoryInternalServices) ImportItems(ctx context.Context, req *pb.ImportItemsReq) (resp *pb.ImportItemsResp, err error) {
	resp, err = i.client.ImportItems(ctx, req)
	return
}

func (i *InventoryInternalServices) UpdateMainFields(ctx context.Context, req *pb.UpdateMainFieldsReq) (resp *pb.UpdateMainFieldsResp, err error) {
	resp, err = i.client.UpdateMainFields(ctx, req)
	return
}

func (i *InventoryInternalServices) GetPoolItems(ctx context.Context, req *pb.GetPoolItemsReq) (resp *pb.GetPoolItemsResp, err error) {
	resp, err = i.client.GetPoolItems(ctx, req)
	return
}

func (i *InventoryInternalServices) RandomItems(ctx context.Context, req *pb.RandomItemsReq) (resp *pb.RandomItemsResp, err error) {
	resp, err = i.client.RandomItems(ctx, req)
	return
}

func (i *InventoryInternalServices) Reward(ctx context.Context, designIds []string) (resp *pb.RewardItemsResp, err error) {
	resp, err = i.client.RewardItems(ctx, &pb.RewardItemsReq{DesignIds: designIds})
	return
}

func (i *InventoryInternalServices) RewardItems(ctx context.Context, req *pb.RewardItemsReq) (resp *pb.RewardItemsResp, err error) {
	resp, err = i.client.RewardItems(ctx, req)
	return
}

func (i *InventoryInternalServices) RewardItemsV2(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error) {
	resp, err = i.client.RewardItemsV2(ctx, req)
	return
}
func (i *InventoryInternalServices) FindAndRewardItems(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error) {
	resp, err = i.client.FindAndRewardItems(ctx, req)
	return
}
func (i *InventoryInternalServices) ListItem(ctx context.Context, req *pb.ListItemReq) (resp *pb.ListItemResp, err error) {
	resp, err = i.client.ListItem(ctx, req)
	return
}
func (i *InventoryInternalServices) ListItemV2(ctx context.Context, req *pb.ListItemReqV2) (resp *pb.ListItemRespV2, err error) {
	resp, err = i.client.ListItemV2(ctx, req)
	return
}
func (i *InventoryInternalServices) SelectItems(ctx context.Context, req *pb.SelectItemsReq) (resp *pb.SelectItemsResp, err error) {
	resp, err = i.client.SelectItems(ctx, req)
	return
}
func (i *InventoryInternalServices) DeleteItems(ctx context.Context, req *pb.DeleteItemsReq) (resp *pb.DeleteItemsResp, err error) {
	resp, err = i.client.DeleteItems(ctx, req)
	return
}
func (i *InventoryInternalServices) UpgradeLevel(ctx context.Context, req *pb.UpgradeLevelReq) (resp *pb.UpgradeLevelResp, err error) {
	resp, err = i.client.UpgradeLevel(ctx, req)
	return
}
func (i *InventoryInternalServices) UpgradeRarity(ctx context.Context, req *pb.UpgradeRarityReq) (resp *pb.UpgradeRarityResp, err error) {
	resp, err = i.client.UpgradeRarity(ctx, req)
	return
}
func (i *InventoryInternalServices) UpdateMeta(ctx context.Context, req *pb.UpdateMetaReq) (resp *pb.UpdateMetaResp, err error) {
	resp, err = i.client.UpdateMeta(ctx, req)
	return
}

func (i *InventoryInternalServices) PutAttributes(ctx context.Context, req *pb.PutAttributesReq) (resp *pb.PutAttributesResp, err error) {
	resp, err = i.client.PutAttributes(ctx, req)
	return
}
func (i *InventoryInternalServices) PutLevel(ctx context.Context, req *pb.PutLevelReq) (resp *pb.PutLevelResp, err error) {
	resp, err = i.client.PutLevel(ctx, req)
	return
}
func (i *InventoryInternalServices) UpgradeExp(ctx context.Context, req *pb.UpgradeExpReq) (resp *pb.UpgradeExpResp, err error) {
	resp, err = i.client.UpgradeExp(ctx, req)
	return
}
func (i *InventoryInternalServices) GetTotalItems(ctx context.Context) (total int64, err error) {
	_resp, err := i.client.GetTotalItems(ctx, &pb.GetTotalItemsRequest{})
	if err != nil {
		return
	}
	total = _resp.Total
	return
}
func (i *InventoryInternalServices) GetListSharedItems(ctx context.Context, req *pb.GetListSharedItemsRequest) (resp *pb.GetListSharedItemsResponse, err error) {
	resp, err = i.client.GetListSharedItems(ctx, req)
	return
}

func (i *InventoryInternalServices) GetExpiredSharedItems(ctx context.Context, req *pb.GetExpiredSharedItemsRequest) (resp *pb.GetExpiredSharedItemsResponse, err error) {
	resp, err = i.client.GetExpiredSharedItems(ctx, req)
	return
}

func (i *InventoryInternalServices) RewardSharedItems(ctx context.Context, req *pb.RewardSharedItemsReq) (resp *pb.RewardSharedItemsResp, err error) {
	resp, err = i.client.RewardSharedItems(ctx, req)
	return
}

func (i *InventoryInternalServices) InsertItemDesign(ctx context.Context, req *pb.UploadItemDesignPub) (resp *pb.Empty, err error) {
	resp, err = i.client.InsertItemDesign(ctx, req)
	return
}

func (i *InventoryInternalServices) RefreshDesignModel(ctx context.Context, req *pb.RefreshDesignModelReq) (resp *pb.RefreshDesignModelResp, err error) {
	resp, err = i.client.RefreshDesignModel(ctx, req)
	return
}

func (i *InventoryInternalServices) RewardPooledItemsV2(ctx context.Context, req *pb.RewardItemsV2Req) (resp *pb.RewardItemsV2Resp, err error) {
	resp, err = i.client.RewardPooledItemsV2(ctx, req)
	return
}
